#!/usr/bin/micropython
import os, sys
import json

INTERACTIVE=False
NAMING_DIR = '/lib/naming'

def help(code=0):
    print('rebmuntrap.py [-ih] NAMING GLOSSARY DIR\n', \
          'ARGUMENTS:\n', \
          '\t -i|--interactive : Interactive input of capabilities')
    sys.exit(code)

if '/lib/rebmuntrap.py' in sys.argv:
    sys.argv.remove('/lib/rebmuntrap.py')

for arg in sys.argv.copy():
    if str(arg).startswith('-') or str(arg).startswith('--'):
        if arg in ('-i', '--interactive'):
            INTERACTIVE = True
            sys.argv.pop(0)
        if arg in ('-h', '--help'):
            help(0)
    else:
        # Position argument
        NAMING_DIR = str(arg)
        sys.argv.pop(0)
        break

if len(sys.argv) > 0:
    print("Invalid arguments\n")
    help(1)

symbols = {}
partnumber = []
for lib in os.listdir(NAMING_DIR):
    if not lib.endswith(".json"):
        print(lib, "is not a json")
        continue
    with open('/'.join((NAMING_DIR, lib))) as file:
        _n_json = json.loads(file.read())
        parser = _n_json.get('parser', None)
        if parser is not 'partnumber':
            print(lib, "is not valid partnumber dictionary")
            continue
        for j in _n_json['properties']:
            symbols.update({j['position']: j})

for s in sorted(symbols):
    symbol = symbols[s]
    _q = symbol.get('description', None)
    _e = symbol.get('env', None)
    _n = symbol.get('noneValue', None)
    if not _e: break
    while True:
        _variants = symbol.get('values', {})
        if INTERACTIVE:
            print("\n%s [%s]" % (_q,_e) if _q else _e)
            print('[', ' / '.join(sorted(_variants.values())), ']')
            _v = input()
            print("input:", _v)
        else:
            _v = os.getenv(_e)

        if not _v:
            partnumber.append('0')
            break

        if not INTERACTIVE and (_v not in _variants.values() and str(_v) != str(_n)):
            print("Bad %s value!" % _e)
            sys.exit(1)

        try:
            partnumber.append(next(str(__i) for __i, __j in _variants.items() if str(__j) == str(_v)))
        except StopIteration:
            partnumber.append('0')
        break

print(''.join(partnumber))
