#ifndef _GPIO_LV_H
#define _GPIO_LV_H

#include <linux/gpio.h>
#include <linux/of_gpio.h>

#include <linux/workqueue.h>

struct gpio_lv_pin {
	/* Configuration parameters */
	int input_pin; 		/* Input gpio pin number */
	int direction_pin; 	/* Direction gpio pin number */
	char *name;
	enum of_gpio_flags input_flags;    
	enum of_gpio_flags direction_flags;
	int claimed;		/* Is pin acquired by driver */
	struct {
		int hwirq;
		int irq;
		u8 sense;
		u8 value;
		int debounce;
		int timestamp;
	} irq;
	/* Matthew modfications */
	struct delayed_work work;
	struct gpio_chip *chip;
	bool bidir; /* If pin have a bidirectional mode */
	bool output; /* Defaule mode if no direction pin on system */
};

struct gpio_lv_platform_data {
	struct gpio_lv_pin *pins;
	int npins;
	struct gpio_chip* chip;
	struct irq_domain* irq_domain;
};

struct gpio_lv {
	struct gpio_chip chip;
	struct device *dev;
};

#endif
