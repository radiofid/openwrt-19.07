#ifndef __LINUX_PROTECT_H
#define __LINUX_PROTECT_H

#include <linux/types.h>

struct protect_req;
struct protect_event;

#define PROTECT_MAGIC 'P'
#define SETCTRL_NO 0x00
#define GETCTRL_NO 0x01
#define GETSTAT_NO 0x02
#define PROTECT_SETCTRL _IOW(PROTECT_MAGIC, SETCTRL_NO, struct protect_req *)
#define PROTECT_GETCTRL _IOWR(PROTECT_MAGIC, GETCTRL_NO, struct protect_req *)
#define PROTECT_GETSTAT _IOWR(PROTECT_MAGIC, GETSTAT_NO, struct protect_req *)

struct protect_req {
	__u16 ln;
	__u16 val;
};

struct protect_event {
	__u64 ts;
	__u64 sn;
	__u16 ln;
};

#endif
