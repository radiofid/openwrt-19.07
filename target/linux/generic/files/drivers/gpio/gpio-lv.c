/*

Driver for GPIO through levelshifters using two pins from SoC for direction and value

*/
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/gpio-lv.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/skbuff.h>
#include <linux/ktime.h>
#include <linux/timekeeping.h>

#define LV_IRQ_SENSE_RISE 0x01
#define LV_IRQ_SENSE_FALL 0x02
#define LV_IRQ_SENSE_BOTH (LV_IRQ_SENSE_FALL | LV_IRQ_SENSE_RISE)

/* If you going to work with this driver be ready to suffer. Matt. */

static irqreturn_t gpio_lv_thread_fn(int irq, void *dev_id);

static int deffered = 0;

struct gpio_event {
	struct gpio_chip*       chip;
	char*                   gpio;
	int                     action;
	int                     debounce;
	u64                     timestamp;
	int                     value;
	struct work_struct      work;
	struct sk_buff*         skb;
};

static int gpio_lv_add_var(struct gpio_event* event, int argv, const char* format, ...)
{
	static char buf[128];
	char* s;
	va_list args;
	int len;
	if (argv)
		return 0;

	va_start(args, format);
	len = vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	if (len >= sizeof(buf)) {
		return -ENOMEM;
	}

	s = skb_put(event->skb, len + 1);
	strcpy(s, buf);
	return 0;
}

#define LV_CHECK_RET(ret, label) do { if (ret) goto label; } while(0)
static void gpio_lv_event_work(struct work_struct* ws)
{
	struct gpio_event* ge;
	ge = container_of(ws, struct gpio_event, work);

	int ret = 0;
	char *action = "BOTH";
	if (ge->action == 1)
		action = "RISE";
	else if (ge->action == 2)
		action = "FALL";

	ge->skb = alloc_skb(2048, GFP_KERNEL);
	LV_CHECK_RET(!ge->skb, free_ge);
	ret = gpio_lv_add_var(ge, 0, "HOME=%s","/");
	LV_CHECK_RET(ret, free_skb);
	ret = gpio_lv_add_var(ge, 0, "PATH=%s","/sbin:/bin:/usr/sbin:/usr/bin");
	LV_CHECK_RET(ret, free_skb);
	ret = gpio_lv_add_var(ge, 0, "SUBSYSTEM=%s", "io");
	LV_CHECK_RET(ret, free_skb);
	ret = gpio_lv_add_var(ge, 0, "GPIO=%s", ge->gpio);
	LV_CHECK_RET(ret, free_skb);
	ret = gpio_lv_add_var(ge, 0, "TRIGGER=%s", action);
	LV_CHECK_RET(ret, free_skb);
	ret = gpio_lv_add_var(ge, 0, "DEBOUNCE=%i", ge->debounce);
	LV_CHECK_RET(ret, free_skb);
	ret = gpio_lv_add_var(ge, 0, "TIMESTAMP=%lli", ge->timestamp);
	LV_CHECK_RET(ret, free_skb);
	ret = gpio_lv_add_var(ge, 0, "VALUE=%i", ge->value);
	LV_CHECK_RET(ret, free_skb);


	/* sk_buff memory frees inside of broadcast_uevent */
	int res = broadcast_uevent(ge->skb, 0, 1, GFP_KERNEL);
	if (res)
		printk(KERN_INFO "Error broadcasting\n");

free_skb:
	if (ret) {
		kfree_skb(ge->skb);
	}

free_ge:
	kfree(ge);
}
#undef LV_CHECK_RET

int gpio_lv_create_event(struct gpio_chip* chip, char* gpio, int action, unsigned debounce, unsigned timestamp, int value)
{
	struct gpio_event* ge;
	ge = kzalloc(sizeof(struct gpio_event), GFP_KERNEL);
	/* ge freed in work function */
	if (!ge)
		return -ENOMEM;

	ge->chip = chip;
	ge->gpio = gpio;
	ge->action = action;
	ge->debounce = debounce;
	ge->timestamp = timestamp;
	ge->value = value;
	INIT_WORK(&ge->work, (void*)gpio_lv_event_work);
	schedule_work(&ge->work);
	return 0;
}

int gpio_lv_get_direction(struct gpio_chip *chip, unsigned offset )
{
	struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->parent);
	struct gpio_lv_pin* pin = &pdata->pins[offset];

	dev_dbg(chip->parent, "get direction (%d) pin input(%d) direction(%d)", offset,
			pin->input_pin, pin->direction_pin);

	if (!pin->bidir)
		return gpiod_get_direction(gpio_to_desc(pin->input_pin));

	return !gpio_get_value_cansleep( pin->direction_pin );
}

int gpio_lv_dir_input(struct gpio_chip *chip, unsigned offset )
{
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->parent);
	struct gpio_lv_pin* pin = &pdata->pins[offset];


	dev_dbg(chip->parent, "set direction input (%d) pin input(%d) direction(%d)",
			offset, pin->input_pin, pin->direction_pin);

	if (pin->bidir) {
		dev_dbg(chip->parent, "Changing dirpin state");
		gpio_set_value_cansleep(pin->direction_pin, 0);
	}

	gpio_direction_input(pin->input_pin);
	return 0;
}

int gpio_lv_dir_output(struct gpio_chip *chip, unsigned offset, int value )
{
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->parent);
	struct gpio_lv_pin* pin = &pdata->pins[offset];

	if (!pin->bidir) {
		return -EPERM;
	}

	dev_dbg(chip->parent, "set direction output (%d) pin input(%d) direction(%d)",
			offset, pin->input_pin, pin->direction_pin);
	gpio_set_value_cansleep( pin->direction_pin, 1);
	gpio_direction_output( pin->input_pin, value);

	return 0;
}

int gpio_lv_get_value(struct gpio_chip *chip, unsigned offset )
{
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->parent);
	if (pdata->pins[offset].input_flags == 1) {
		return !gpio_get_value_cansleep( pdata->pins[offset].input_pin);
	} else {
		return gpio_get_value_cansleep( pdata->pins[offset].input_pin);
	}
}

void gpio_lv_set_value(struct gpio_chip *chip, unsigned offset, int value )
{
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->parent);

	if (pdata->pins[offset].input_flags == 1) {
		gpio_set_value_cansleep( pdata->pins[offset].input_pin, !value);
	} else {
		gpio_set_value_cansleep( pdata->pins[offset].input_pin, value);
	}
}

static int gpio_lv_request(struct gpio_chip *chip, unsigned offset)
{
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->parent);

	if( pdata == NULL ) {
		dev_err(chip->parent, "Platform data return NULL." );
		return -EFAULT;
	}

	struct gpio_lv_pin* pin = &pdata->pins[offset];

	dev_dbg(chip->parent, "Request (%d) pin input(%d) output(%d)", offset, 
			pin->input_pin, pin->direction_pin);

	if( gpio_request(pin->input_pin, NULL) != 0 ) {
		dev_err(chip->parent, "Input pin(%d) in lv-pin %d already in use", 
				pin->input_pin, offset);
		return -EBUSY;
	}

	if (pin->bidir) {
		if( gpio_request(pin->direction_pin, NULL) != 0 ) {
			gpio_free(pin->input_pin);
			dev_err(chip->parent, "Direction pin(%d) in lv-pin %d already in use",
				pin->direction_pin, offset);
			return -EBUSY;
		}
		gpio_direction_output(pin->direction_pin, 0);
	}

	pin->claimed = 1;

	gpiod_set_debounce(gpio_to_desc(chip->base + offset), 100);
	return 0;
}

void gpio_lv_free(struct gpio_chip *chip, unsigned offset)
{
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(chip->parent);
	struct gpio_lv_pin* pin = &pdata->pins[offset];

	if( pin->claimed ) {
		gpio_free(pin->input_pin);
		if (pin->bidir)
			gpio_free(pin->direction_pin);
		pin->claimed = 0;
	}
}

static int gpio_lv_to_irq( struct gpio_chip *chip, unsigned offset )
{
	struct gpio_lv *lv = container_of(chip, struct gpio_lv, chip);
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(lv->dev);
	int irq;
	if (pdata->irq_domain) {
		irq = irq_create_mapping(pdata->irq_domain, offset);
		dev_info(chip->parent, "Created irq map %d on %d", irq, offset);
	}
	else {
		dev_err(chip->parent, "Failed to map interrupt");
		return -ENXIO;
	}

	pdata->pins[offset].irq.irq = irq;
	return irq;
}

static int gpio_lv_set_config(struct gpio_chip *chip,
				   unsigned offset, unsigned long config)
{
	struct gpio_lv* lv = container_of(chip, struct gpio_lv, chip);
	const struct gpio_lv_platform_data* pdata = dev_get_platdata(lv->dev);
	unsigned long param = pinconf_to_config_param(config);
	struct gpio_lv_pin *pin = (struct gpio_lv_pin*)&pdata->pins[offset];
	u32 arg = pinconf_to_config_argument(config);

	if (param != PIN_CONFIG_INPUT_DEBOUNCE)
		return -ENOTSUPP;

	pin->irq.debounce = arg;
	return 0;
}

static int gpio_lv_irq_set_type(struct irq_data *d, unsigned int flow_type)
{
	struct gpio_lv *lv = irq_data_get_irq_chip_data(d);
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(lv->dev);
	int offset = -1;
	unsigned val = 0;
	int i;

	if (flow_type & (IRQ_TYPE_LEVEL_HIGH | IRQ_TYPE_LEVEL_LOW))
		return -EINVAL;

	for(i = 0; i < pdata->npins; ++i) {
		if (pdata->pins[i].irq.irq == d->irq) {
			offset = i;
		}
	}
	if (offset < 0)
		return 0;

	if (flow_type & IRQ_TYPE_EDGE_RISING)
		val |= 0x1;
	if (flow_type & IRQ_TYPE_EDGE_FALLING)
		val |= 0x2;

	pdata->pins[offset].irq.sense = val;
	if (pdata->pins[offset].input_flags == 1) {
		pdata->pins[offset].irq.value = !gpio_get_value_cansleep(pdata->pins[offset].input_pin);
	} else {
		pdata->pins[offset].irq.value = gpio_get_value_cansleep(pdata->pins[offset].input_pin);
	}

	return 0;
}

static irqreturn_t debounce_irq_handler(int irq, void *p)
{
	struct gpio_lv_pin *pin = p;

	if (!(READ_ONCE(pin->irq.sense) & LV_IRQ_SENSE_BOTH)) {
		return IRQ_NONE;
	}

	pin->irq.timestamp = ktime_to_ms(ktime_get_boottime());

	mod_delayed_work(system_wq, &pin->work,
		msecs_to_jiffies(READ_ONCE(pin->irq.debounce)));

	return IRQ_HANDLED;
}

static void debounce_work_func(struct work_struct *work)
{
	struct gpio_lv_pin *pin = container_of(work, struct gpio_lv_pin, work.work);

	u8 oldval = pin->irq.value;
	pin->irq.value = gpiod_get_value_cansleep(gpio_to_desc(pin->input_pin));
	if (pin->input_flags == 1)
		WRITE_ONCE(pin->irq.value, !pin->irq.value);

	if (oldval == READ_ONCE(pin->irq.value))
		return;

	u8 sense = READ_ONCE(pin->irq.sense);
	if (oldval > READ_ONCE(pin->irq.value) && (sense & LV_IRQ_SENSE_FALL) ||
	    oldval < READ_ONCE(pin->irq.value) && (sense & LV_IRQ_SENSE_RISE)) {
		gpio_lv_create_event(
			pin->chip,
			pin->name,
			pin->irq.sense,
			pin->irq.debounce,
			pin->irq.timestamp,
			pin->irq.value
		);
	}
}

static void gpio_lv_irq_enable(struct irq_data* d)
{
	struct gpio_lv *lv = irq_data_get_irq_chip_data(d);
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(lv->dev);
	struct gpio_lv_pin* pin = NULL;
	int offset = -1;
	int irq = 0;
	int i, ret;
	unsigned long irqflags;

	for(i = 0; i < pdata->npins; ++i) {
		if (pdata->pins[i].irq.irq == d->irq) {
			offset = i;
		}
	}
	if (offset < 0)
		return;

	pin = &pdata->pins[offset];

	/* This check must be done atomically to prevent race conditions */
	if (pin->irq.hwirq < 0) {
		irq = gpio_to_irq( pin->input_pin);
		if (irq < 0) {
			dev_err (lv->dev, "irq pin(%d) not valid", offset);
			return;
		}
		irqflags = IRQF_ONESHOT | IRQF_TRIGGER_RISING |
			   IRQF_TRIGGER_FALLING;
		/* 
		 * Use "any_context", becase type of interrupt handler depends
		 * on real irqchip
		 */
		ret = request_any_context_irq(irq, debounce_irq_handler,
					irqflags,
					pin->name, pin);
		if (ret < 0) {
			dev_err (lv->dev, "irq request failed error (%i)\n", ret);
			return;
		}
		pin->irq.hwirq = irq;
	}
}

static void gpio_lv_irq_disable(struct irq_data* d)
{
	struct gpio_lv *lv = irq_data_get_irq_chip_data(d);
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(lv->dev);
	struct gpio_lv_pin* pin = NULL;
	int offset = -1;
	int i;
	for(i = 0; i < pdata->npins; ++i) {
		if (pdata->pins[i].irq.irq == d->irq) {
			offset = i;
		}
	}
	if (offset < 0)
		return;
	pin = &pdata->pins[offset];

	pin->irq.sense = 0;
	free_irq(gpio_to_irq(pin->input_pin), pin);
	pin->irq.hwirq = -1;
}

static struct irq_chip gpio_lv_irq_chip = {
	.name = "gpio-lv",
	.irq_set_type = gpio_lv_irq_set_type,
	.irq_enable   = gpio_lv_irq_enable,
	.irq_disable  = gpio_lv_irq_disable,
};

static int gpio_lv_irq_map(struct irq_domain* h, unsigned int irq,
		irq_hw_number_t hwirq)
{
	irq_set_chip_data(irq, h->host_data);
	irq_set_chip_and_handler(irq, &gpio_lv_irq_chip, handle_edge_irq);
	irq_set_nested_thread(irq, 1);
	return 0;
}

static const struct irq_domain_ops gpio_lv_domain_ops = {
	.map = gpio_lv_irq_map,
	.xlate = irq_domain_xlate_twocell,
};

static int gpio_lv_probe(struct platform_device *pdev)
{
	int ret = 0;
	struct device *dev = &pdev->dev;
	struct gpio_lv_platform_data *pdata = NULL;
	struct device_node *root = dev->of_node;
	struct device_node *child;
	struct gpio_lv_pin* pins;
	struct gpio_lv* chip;
	int i;

	if ( !root ) {
		dev_err(dev, "No device tree data available\n");
		return -EINVAL;
	}

	chip = devm_kzalloc(dev, sizeof( struct gpio_lv ), GFP_KERNEL);
	if( !chip ) {
		dev_err(dev, "failed to allocate lv_chip struct\n");
		return -ENOMEM;
	}

	dev->platform_data = devm_kzalloc( dev,
			sizeof( struct gpio_lv_platform_data ), GFP_KERNEL);

	pdata = dev->platform_data;

	/* Get count of declared pins*/
	pdata->npins = of_get_child_count(root);

	pins = devm_kzalloc( dev, sizeof( struct gpio_lv_pin ) * pdata->npins,
			GFP_KERNEL);
	if( !pins ) {
		dev_err(dev, "failed to allocate pins struct\n");
		return -ENOMEM;
	}
	/* Set chip info */
	chip->chip.label		= dev_name(dev);
	chip->chip.can_sleep		= true;
	chip->chip.owner		= THIS_MODULE;
	chip->chip.base			= -1;
	chip->chip.of_node		= root;
	chip->dev			= &pdev->dev;
	chip->chip.parent		= &pdev->dev;
	chip->chip.ngpio		= pdata->npins;
	/* set handlers for gpio operations*/
	chip->chip.direction_input	= gpio_lv_dir_input;
	chip->chip.direction_output	= gpio_lv_dir_output;
	chip->chip.get			= gpio_lv_get_value;
	chip->chip.set			= gpio_lv_set_value;
	chip->chip.set_config		= gpio_lv_set_config;
	chip->chip.to_irq		= gpio_lv_to_irq;
	chip->chip.get_direction	= gpio_lv_get_direction;
	chip->chip.request		= gpio_lv_request;
	chip->chip.free			= gpio_lv_free;

	i = 0;
	for_each_child_of_node(root, child) {
		struct gpio_lv_pin* pin = pins + i;

		/*Get pin info from device tree record*/
		pin->input_pin      = of_get_named_gpio_flags(child,
				"input_pin", 0, &(pin->input_flags));
		pin->direction_pin  = of_get_named_gpio_flags(child,
				"direction_pin", 0, &(pin->direction_flags));

		ret = of_property_read_string(child, "name", (const char**)&pin->name);
		if (ret) {
			dev_err(dev, "Not found gpio-name in dts\n");
			goto err;
		}
		
		pin->bidir = true;

		/* Initialize and check pins */
		if( gpio_is_valid(pin->input_pin) == false ){
			if ( !deffered ) {
				deffered = 1;
				return -EPROBE_DEFER;
			}
			dev_err(dev, "Input pin(%d) in %d pin not valid",pin->input_pin, i);
			return -EINVAL;
		}

		if( gpio_is_valid(pin->direction_pin) == false ){
			if ( ! deffered ) {
				deffered = 1;
				return -EPROBE_DEFER;
			}
			dev_info(dev, "Direction pin in %d pin not found",
				pin->direction_pin, i);
			pin->bidir = false;
		}

		if (!pin->bidir) {
			dev_info(dev, "Pin %d configured as input", pin->input_pin);
			gpio_direction_input(pin->input_pin);
		}

		dev_info( dev, "gpio-lv pin %d, input(%d)-out(%d) dir(%d) flags(%s)", i,
				pin->input_pin,
				pin->output,
				pin->direction_pin,
				(pin->input_flags == 0x01) ? "GPIO_ACTIVE_LOW" : "GPIO_ACTIVE_HIGH");

		pin->irq.hwirq = -1;
		pin->irq.sense = 0;
		pin->irq.debounce = 10;
		pin->irq.timestamp = 0;
		pin->chip = &chip->chip;
		INIT_DELAYED_WORK(&pin->work, debounce_work_func);

		i++;
	}

	pdata->pins = pins;

	/* register gpio chip */
	ret = gpiochip_add_data(&chip->chip, chip);
	platform_set_drvdata(pdev, chip);

	pdata->irq_domain = irq_domain_add_linear(root, pdata->npins, &gpio_lv_domain_ops, chip);
	if (!pdata->irq_domain) {
		dev_err(dev, "Could not add irq_domain\n");
		return -EINVAL;
	}
	dev_info(dev, "gpiochip-add gpio.base(%i)\n", chip->chip.base);
err:
	return ret;
}

static int gpio_lv_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	const struct gpio_lv_platform_data *pdata = dev_get_platdata(dev);
	int i;

	for( i=0; i < pdata->npins; i++ ){
		struct gpio_lv_pin* pin = &(pdata->pins[i]);

		if( pin->claimed ){
			gpio_free(pin->input_pin);
			if (pin->bidir)
				gpio_free(pin->direction_pin);
			pin->claimed = 0;
		}
	}

	return 0;
}

static const struct of_device_id gpio_lv_of_match[] = {
	{ .compatible = "gpio-lv" },
	{},
};


static struct platform_driver gpio_lv_device_driver = {
	.probe		= gpio_lv_probe,
	.remove		= gpio_lv_remove,
	.driver		= {
		.name	= "gpio-lv",
		.owner	= THIS_MODULE,
		.of_match_table = of_match_ptr(gpio_lv_of_match),
	}
};


MODULE_DEVICE_TABLE(of, gpio_lv_of_match);

static int __init gpio_lv_init(void)
{
	return platform_driver_register(&gpio_lv_device_driver);
}

static void __exit gpio_lv_exit(void)
{
	platform_driver_unregister(&gpio_lv_device_driver);
}

device_initcall(gpio_lv_init);
module_exit(gpio_lv_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Nikita Nazarenko <nnazarenko@radiofid.com>");
MODULE_AUTHOR("Matthew Bystrin <mbystrin@radiofid.com>");
MODULE_DESCRIPTION("Wrapper GPIO driver over LV*** levelshifter");
MODULE_ALIAS("platform:gpio-lv");
