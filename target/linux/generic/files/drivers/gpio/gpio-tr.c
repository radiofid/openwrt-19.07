/*

Driver for GPIO through levelshifters using two pins from SoC for direction and value

*/
#include <linux/init.h>
#include <linux/gpio.h>
#include <linux/module.h>
#include <linux/device.h>
#include <linux/platform_device.h>
#include <linux/slab.h>
#include <linux/gpio-tr.h>
#include <linux/of.h>
#include <linux/of_gpio.h>
#include <linux/interrupt.h>
#include <linux/delay.h>
#include <linux/skbuff.h>
#include <linux/ktime.h>
#include <linux/timekeeping.h>

static int deffered = 0;


struct gpio_event {
	struct gpio_chip*       chip;
	char*                   gpio;
	int                     action;
	int                     debounce;
	u64                     timestamp;
	int                     value;
	struct work_struct      work;
	struct sk_buff*         skb;
};


static int gpio_tr_add_var(struct gpio_event* event, int argv, const char* format, ...)
{
	static char buf[128];
	char* s;
	va_list args;
	int len;
	if (argv)
		return 0;

	va_start(args, format);
	len = vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	if (len >= sizeof(buf)) {
		return -ENOMEM;
	}

	s = skb_put(event->skb, len + 1);
	strcpy(s, buf);
	return 0;
}


static void gpio_tr_event_work(struct work_struct* ws)
{
	struct gpio_event* ge;
	int ret = 0;

	ge = container_of(ws, struct gpio_event, work);

	ge->skb = alloc_skb(2048, GFP_KERNEL);
	if (!ge->skb)
		goto free_ge;
	
	ret = gpio_tr_add_var(ge, 0, "HOME=%s","/");
	if(ret)
		goto free_skb;
	
	ret = gpio_tr_add_var(ge, 0, "PATH=%s","/sbin:/bin:/usr/sbin:/usr/bin");
	if(ret)
		goto free_skb;

	ret = gpio_tr_add_var(ge, 0, "SUBSYSTEM=%s", "io");
	if(ret)
		goto free_skb;

	ret = gpio_tr_add_var(ge, 0, "GPIO=%s", ge->gpio);
	if(ret)
		goto free_skb;

	ret = gpio_tr_add_var(ge, 0, "TRIGGER=%s", (ge->action == 1) ? "RISE": (ge->action == 2) ? "FALL" : "BOTH");
	if(ret)
		goto free_skb;

	ret = gpio_tr_add_var(ge, 0, "DEBOUNCE=%i", ge->debounce);
	if (ret)
		goto free_skb;

	ret = gpio_tr_add_var(ge, 0, "TIMESTAMP=%lli", ge->timestamp);
	if (ret)
		goto free_skb;

	ret = gpio_tr_add_var(ge, 0, "VALUE=%i", ge->value);
	if (ret)
		goto free_skb;

	broadcast_uevent(ge->skb, 0, 1, GFP_KERNEL);

free_skb:
	if(ret) {
		kfree_skb(ge->skb);
	}

free_ge:
	kfree(ge);
}


int gpio_tr_create_event(struct gpio_chip* chip, char* gpio, int action, unsigned debounce, unsigned timestamp, int value)
{
	struct gpio_event* ge;
	ge = kzalloc(sizeof(struct gpio_event), GFP_KERNEL);
	if (!ge)
		return -ENOMEM;

	ge->chip = chip;
	ge->gpio = gpio;
	ge->action = action;
	ge->debounce = debounce;
	ge->timestamp = timestamp;
	ge->value = value;
	INIT_WORK(&ge->work, (void*)gpio_tr_event_work);
	schedule_work(&ge->work);
	return 0;
}


void gpio_tr_set_value(struct gpio_chip *chip, unsigned offset, int value );


int gpio_tr_get_direction(struct gpio_chip *chip, unsigned offset )
{
	struct gpio_tr *tr = container_of(chip, struct gpio_tr, chip);
	struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);

	return pdata->pins[offset].direction;
}


int gpio_tr_dir_input(struct gpio_chip *chip, unsigned offset )
{
	struct gpio_tr *tr = container_of(chip, struct gpio_tr, chip);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);

	pdata->pins[offset].direction = GPIOF_DIR_IN;

	gpio_set_value_cansleep( pdata->pins[offset].high_pin, 0);
	gpio_set_value_cansleep( pdata->pins[offset].low_pin, 0);

	return 0;
}


int gpio_tr_dir_output(struct gpio_chip *chip, unsigned offset, int value )
{
	struct gpio_tr *tr = container_of(chip, struct gpio_tr, chip);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);

	pdata->pins[offset].direction = GPIOF_DIR_OUT;

	gpio_tr_set_value( chip, offset, value );

	return 0;
}


int gpio_tr_get_value(struct gpio_chip *chip, unsigned offset )
{
	struct gpio_tr *tr = container_of(chip, struct gpio_tr, chip);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);

	return !gpiod_get_value(gpio_to_desc(pdata->pins[offset].input_pin));
}


void gpio_tr_set_value(struct gpio_chip *chip, unsigned offset, int value )
{
	struct gpio_tr *tr = container_of(chip, struct gpio_tr, chip);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);

	if( value ) {
		gpio_set_value_cansleep( pdata->pins[offset].high_pin, 1);
		gpio_set_value_cansleep( pdata->pins[offset].low_pin, 0);
	} else {
		gpio_set_value_cansleep( pdata->pins[offset].high_pin, 0);
		gpio_set_value_cansleep( pdata->pins[offset].low_pin, 1);
	}

	return;
}


static int gpio_tr_request(struct gpio_chip *chip, unsigned offset)
{
	struct gpio_tr *tr = container_of(chip, struct gpio_tr, chip);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);

	if( pdata == NULL ){
		dev_err(tr->dev, "Platform data return NULL." );
		return -EFAULT;
	}

	struct gpio_tr_pin *pin = (struct gpio_tr_pin*)&pdata->pins[offset];

	dev_dbg(tr->dev, "Request (%d) pin input(%d) high(%d) low(%d)", offset, pin->input_pin, pin->high_pin, pin->low_pin);

	if( gpio_request(pin->input_pin, NULL) != 0 ) {
		dev_err(tr->dev, "Input pin(%d) in tr-pin %d already in use", pin->input_pin, offset);
		return -EBUSY;
	}

	if( gpio_request(pin->high_pin, NULL) != 0 ) {
		gpio_free(pin->input_pin);
		dev_err(tr->dev, "High level pin(%d) in tr-pin %d already in use", pin->high_pin, offset);
		return -EBUSY;
	}

	if( gpio_request(pin->low_pin, NULL) != 0 ) {
		gpio_free(pin->input_pin);
		gpio_free(pin->high_pin);
		dev_err(tr->dev, "Low level pin(%d) in tr-pin %d already in use", pin->low_pin, offset);
		return -EBUSY;
	}

	gpio_direction_input(pin->input_pin);
	gpio_direction_output(pin->high_pin, 0);
	gpio_direction_output(pin->low_pin, 0);
	pin->direction = GPIOF_DIR_IN;
	pin->claimed = 1;
	gpiod_set_debounce(gpio_to_desc(chip->base + offset), 100);
	return 0;
}


void gpio_tr_free(struct gpio_chip *chip, unsigned offset)
{
	struct gpio_tr *tr = container_of(chip, struct gpio_tr, chip);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);
	struct gpio_tr_pin* pin = &pdata->pins[offset];

	if( pin->claimed ){
		gpio_free(pin->input_pin);
		gpio_free(pin->high_pin);
		gpio_free(pin->low_pin);
		pin->claimed = 0;
	}

	return;
}

static irqreturn_t gpio_tr_thread_fn(int irq, void *dev_id)
{
	struct gpio_tr *tr = (struct gpio_tr*)dev_id;
	const struct gpio_tr_platform_data* pdata = dev_get_platdata(tr->dev);
	int i = 0, old = 0, offset = -1, sense = 0;
	for (i = 0; i < pdata->npins; ++i) {
		if (irq == pdata->pins[i].irq.hwirq) {
			offset = i;
			break;
		}
	}

	if (offset < 0 || offset >= pdata->npins)
		return IRQ_NONE;
	if (pdata->pins[offset].direction != GPIOF_DIR_IN)
		return IRQ_NONE;

	sense = pdata->pins[offset].irq.sense;
	if (!(sense & 0x1 || sense & 0x2))
		return IRQ_NONE;

	old = pdata->pins[offset].irq.value;
	pdata->pins[offset].irq.value = !gpiod_get_value(gpio_to_desc(pdata->pins[offset].input_pin));
	unsigned old_timestamp = pdata->pins[offset].irq.timestamp;
	pdata->pins[offset].irq.timestamp = ktime_to_ms(ktime_get_boottime());
	if ((pdata->pins[offset].irq.timestamp - old_timestamp) < pdata->pins[offset].irq.debounce) {
		return IRQ_NONE;
	}
	if (((sense & 0x1) && !old) || ((sense & 0x2) && old)) {
		gpio_tr_create_event(
				&tr->chip,
				pdata->pins[offset].name, sense,
				pdata->pins[offset].irq.debounce,
				pdata->pins[offset].irq.timestamp,
				pdata->pins[offset].irq.value);
		handle_nested_irq(pdata->pins[offset].irq.irq);
		return IRQ_HANDLED;
	}
	return IRQ_NONE;
}

static int gpio_tr_to_irq( struct gpio_chip *chip, unsigned offset )
{
	struct gpio_tr *tr = container_of(chip, struct gpio_tr, chip);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);
	int irq;
	if (pdata->irq_domain)
		irq = irq_create_mapping(pdata->irq_domain, offset);
	else
		return -ENXIO;

	pdata->pins[offset].irq.irq = irq;
	return irq;
}

static int gpio_tr_set_config(struct gpio_chip *chip,
				   unsigned offset, unsigned long config)
{
	struct gpio_tr* tr = container_of(chip, struct gpio_tr, chip);
	const struct gpio_tr_platform_data* pdata = dev_get_platdata(tr->dev);
	unsigned long param = pinconf_to_config_param(config);
	struct gpio_tr_pin *pin = (struct gpio_tr_pin*)&pdata->pins[offset];
	u32 arg = pinconf_to_config_argument(config);

	if (param != PIN_CONFIG_INPUT_DEBOUNCE)
		return -ENOTSUPP;
	
	pin->irq.debounce = arg;
	return 0;
}

static int gpio_tr_irq_set_type(struct irq_data *d, unsigned int flow_type)
{
	struct gpio_tr *tr = irq_data_get_irq_chip_data(d);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);
	int offset = -1;
	unsigned val = 0;
	int i;

	if (flow_type & (IRQ_TYPE_LEVEL_HIGH | IRQ_TYPE_LEVEL_LOW))
		return -EINVAL;

	for(i = 0; i < pdata->npins; ++i) {
		if (pdata->pins[i].irq.irq == d->irq) {
			offset = i;
		}
	}
	if (offset < 0)
		return 0;

	if (flow_type & IRQ_TYPE_EDGE_RISING)
		val |= 0x1;
	if (flow_type & IRQ_TYPE_EDGE_FALLING)
		val |= 0x2;

	pdata->pins[offset].irq.sense = val;
	pdata->pins[offset].irq.value = !gpiod_get_value(gpio_to_desc(pdata->pins[offset].input_pin));

	return 0;
}

static void gpio_tr_mask(struct irq_data* d)
{
	struct gpio_tr *tr = irq_data_get_irq_chip_data(d);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);
	int offset = -1;
	int i;
	for(i = 0; i < pdata->npins; ++i) {
		if (pdata->pins[i].irq.irq == d->irq) {
			offset = i;
		}
	}
	if (offset < 0)
		return;

	pdata->pins[offset].irq.sense = 0;
}

static void gpio_tr_unmask(struct irq_data* d)
{
	struct gpio_tr *tr = irq_data_get_irq_chip_data(d);
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(tr->dev);
}


static struct irq_chip gpio_tr_irq_chip = {
	.name = "gpio-tr",
	.irq_set_type = gpio_tr_irq_set_type,
	.irq_mask     = gpio_tr_mask,
	.irq_unmask   = gpio_tr_unmask,
};

static int gpio_tr_irq_map(struct irq_domain* h, unsigned int irq,
		irq_hw_number_t hwirq)
{
	irq_set_chip_data(irq, h->host_data);
	irq_set_chip_and_handler(irq, &gpio_tr_irq_chip, handle_edge_irq);
	irq_set_nested_thread(irq, 1);
	return 0;
}


static const struct irq_domain_ops gpio_tr_domain_ops = {
	.map = gpio_tr_irq_map,
	.xlate = irq_domain_xlate_twocell,
};

static int gpio_tr_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct gpio_tr_platform_data *pdata = NULL;
	struct device_node *root = dev->of_node;
	struct device_node *child;
	struct gpio_tr_pin* pins;
	struct gpio_tr* chip;
	char**	names;
	int irq;

	int i, ret;

	if ( !root ){
	    dev_err(dev, "No device tree data available\n");
	    return -EINVAL;
	}
	
	chip = devm_kzalloc(dev, sizeof( struct gpio_tr ), GFP_KERNEL);
	
	if( !chip ){
		dev_err(dev,"failed to allocate lv_chip struct\n");
		return -ENOMEM;
	}
	
	if( dev->platform_data == NULL){
		dev->platform_data = devm_kzalloc( dev, sizeof( struct gpio_tr_platform_data ), GFP_KERNEL);
	}
	
	pdata = dev->platform_data;
	
	/* Get count of declared pins*/
	pdata->npins = of_get_child_count(root);

	pins = devm_kzalloc( dev, sizeof( struct gpio_tr_pin ) * pdata->npins, GFP_KERNEL);

	if( !pins ){
	    dev_err(dev,"failed to allocate pins struct\n");
	    return -ENOMEM;
	}
	names 		= devm_kzalloc(dev, sizeof(char*) * pdata->npins, GFP_KERNEL);

	if( !names ){
	    dev_err(dev,"failed to allocate names array\n");
	    return -ENOMEM;
	}
	/* set handlers for gpio operations*/
	chip->chip.direction_input	= gpio_tr_dir_input;
	chip->chip.direction_output	= gpio_tr_dir_output;
	chip->chip.get			= gpio_tr_get_value;
	chip->chip.set			= gpio_tr_set_value;
	chip->chip.to_irq		= gpio_tr_to_irq;
	chip->chip.set_config	= gpio_tr_set_config;
	chip->chip.get_direction	= gpio_tr_get_direction;
	chip->chip.request		= gpio_tr_request;
	chip->chip.free			= gpio_tr_free;
#ifdef CONFIG_OF_GPIO
	chip->chip.of_node		= pdev->dev.parent->of_node;
#endif
	

	/* initialize undelying gpios to begin state */
	i = 0;
	
	for_each_child_of_node(root,child) {
	    struct gpio_tr_pin* pin = &(pins[i]);
	    char* name = NULL;
	    /*Get pin info from device tree record*/
		pin->input_pin  = of_get_named_gpio_flags(child, "input_pin", 0, &(pins->input_flags));
		pin->high_pin  	= of_get_named_gpio_flags(child, "high_pin", 0, &(pins->high_flags));
		pin->low_pin	= of_get_named_gpio_flags(child, "low_pin", 0, &(pins->low_flags));
		of_property_read_string( child, "name", (const char**)&name);
		of_property_read_string( child, "name", (const char**)&pin->name);

		/* Initialize and check pins */
		if( gpio_is_valid(pin->input_pin) == false ){
			if ( ! deffered ) {
				deffered = 1;
				return -EPROBE_DEFER;
			}
			dev_err(dev, "Input pin(%d) in %d pin not valid",pin->input_pin, i);
			return -EINVAL;
		} 

		if( gpio_is_valid(pin->high_pin) == false ){
			if ( ! deffered ) {
				deffered = 1;
				return -EPROBE_DEFER;
			}
			dev_err(dev, "High level pin(%d) in %d pin not valid",pin->high_pin, i);
			return -EINVAL;
		}
		
		if( gpio_is_valid(pin->low_pin) == false ){
			if ( ! deffered ) {
				deffered = 1;
				return -EPROBE_DEFER;
			}
			dev_err(dev, "Low level pin(%d) in %d pin not valid",pin->low_pin, i);
			return -EINVAL;
		}
		names[i] = name;
		dev_info( dev, "gpio-tr pin %d(%s), input(%d) high(%d) low(%d)", i,name ? name : "noname", pin->input_pin, pin->high_pin, pin->low_pin );

		irq = gpio_to_irq(pin->input_pin);
		if (irq < 0) {
			dev_err (dev, "irq pin(%d) not valid", i);
			goto err_irq;
		}
		ret = request_threaded_irq(irq, NULL, gpio_tr_thread_fn,
				IRQF_NO_THREAD | IRQF_ONESHOT | IRQF_TRIGGER_RISING | IRQF_TRIGGER_FALLING,
				"gpio", chip);
		if (ret) {
			dev_err (dev, "request_threaded_irq");
		}
		pin->irq.hwirq = irq;
		pin->irq.sense = 0;
		pin->irq.debounce = 10;
		pin->irq.timestamp = 0;
err_irq:
		i++;
	}
	
	pdata->pins = pins;
	chip->chip.ngpio = pdata->npins;
	chip->chip.names 	= (const char* const*)names;
	chip->chip.can_sleep = 1;
	chip->chip.owner	= THIS_MODULE;
	chip->chip.base = -1; /* this is "virtual" gpio chip, so we aren't need to set base num explicitly.*/
	chip->chip.parent = &pdev->dev;
	chip->chip.label = "gpio-tr";
	chip->dev = &pdev->dev;

	pdata->irq_domain = irq_domain_add_linear(root, pdata->npins, &gpio_tr_domain_ops, chip);
	if (!pdata->irq_domain) {
		dev_err(dev, "Could bot add irq_domain\n");
		return -EINVAL;
	}

	/* register gpio chip */
	ret = gpiochip_add( &chip->chip );

	return ret;
}


static int gpio_tr_remove(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	const struct gpio_tr_platform_data *pdata = dev_get_platdata(dev);
	int i;
	
	for( i=0; i < pdata->npins; i++ ){
	    struct gpio_tr_pin* pin = &(pdata->pins[i]);
	    
	    if( pin->claimed ){
		    gpio_free(pin->input_pin);
		    gpio_free(pin->high_pin);
		    gpio_free(pin->low_pin);
		    pin->claimed = 0;
	    }
	}
	
//	kfree( pdata->pins );
//	kfree( pdata->chip );
//	kfree( pdata );
	
	return 0;
}


static const struct of_device_id gpio_tr_of_match[] = {
	{ .compatible = "gpio-tr" },
	{},
};


static struct platform_driver gpio_tr_device_driver = {
	.probe		= gpio_tr_probe,
	.remove		= gpio_tr_remove,
	.driver		= {
		.name	= "gpio-tr",
		.owner	= THIS_MODULE,
		.of_match_table = of_match_ptr(gpio_tr_of_match),
	}
};


MODULE_DEVICE_TABLE(of, gpio_tr_of_match);

static int __init gpio_tr_init(void)
{
	return platform_driver_register(&gpio_tr_device_driver);
}


static void __exit gpio_tr_exit(void)
{
	platform_driver_unregister(&gpio_tr_device_driver);
}


device_initcall(gpio_tr_init);
module_exit(gpio_tr_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Dmitry Peresypkin <dperesypkin@radiofid.com>");
MODULE_DESCRIPTION("Wrapper GPIO driver over some logic");

MODULE_ALIAS("platform:gpio-tr");
