/*
 * IRZ GPIO-HL driver
 *
 * HL stands for HIGH-LOW. Driver creates a "virtual" gpio controller on top of
 * existing one. It uses 2 swithes: high and low to handle gpio output. And use
 * one input gpio to serve as is.
 *
 *					       +V
 *						|
 *		  .----HIGH SWITCH--(out)------\
 *		 /				|
 *	REAL GPIO--------INPUT-----(input)------+------> INPUT/OUTPUT
 *		 \				|
 *		  '-----LOW SWITCH--(out)------\
 *						|
 *					       GND
 *
 * Driver do not cares about real gpio. It can be either SoC pins
 * or an I2C expander (or other gpio as well). This driver needs 3 gpio pins:
 * 1. Output pin to control high switch
 * 2. Output pin to control low switch
 * 3. Input pin to read input
 *
 * Another important feature of this driver that it saves the state of pins
 * between reboots. Still it has some limitations
 */

#include <linux/bitmap.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/irqchip.h>
#include <linux/irqdomain.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/of_platform.h>
#include <linux/platform_device.h>
#include <linux/workqueue.h>

#include <linux/gpio/consumer.h>
#include <linux/gpio/driver.h>
#include <linux/gpio/machine.h>
#include <linux/of_gpio.h>
#include <linux/spinlock.h>

#include <linux/err.h>
#include <linux/string.h>

#include "gpiolib.h"

#define get_vchip(gchip) container_of(gchip, struct gpio_vchip, chip);

#define for_each_vline(vchip, vl)                                          \
	for ((vl) = (vchip)->lines; (vl) < (vchip)->lines + (vchip)->lnum; \
	     (vl)++)

#define DEFAULT_DEBOUNCE 100

/*
 * struct vline - abstraction line for gpio-hl chip
 * @low: low switch gpio descriptor, used to pull line to groud
 * @high: high switch gpio descriptor, used to pull line to vcc
 * @in: in gpio descriptor, used to read line state
 * @flags: bitarray to store gpio flags
 * @irqflags: bitarray to store irq flags
 * @irq: irq number associated with vline
 * @value: old value of line, used in software debouce
 * @irq_unmasked: bool flag showing if hardware debounce available in this vline
 * @dwork: work launched after debouce period expired
 */
struct vline {
	struct gpio_desc *low;
	struct gpio_desc *high;
	struct gpio_desc *in;

	const char *name;

	unsigned long flags;
#define VGPIO_VALUE 0
#define VGPIO_IS_OUT 1
	unsigned long irqflags;

	int irq;
	int value;
	bool irq_unmasked;
	bool hw_debounce;

	struct delayed_work dwork;
};

/*
 * struct gpio_hl_vchip - virtual chip
 * @chip: the GPIO device which used to register virtual chip
 * @lines: lines owned by this virtual chip
 * @lnum: number of virtual lines
 */
struct gpio_vchip {
	struct gpio_chip chip;
	struct vline *lines;
	u32 lnum;
};

/*
 * struct event_data - hotplug event data
 * @name: name of the gpio
 * @trig: interrupt trigger value - RISE, FALL or BOTH
 * @debounce: line debounce value in ms
 * @timestamp: interrupt timestamp
 * @value: line value
 */
struct event_data {
	const char *name;
	const char *trig;
	unsigned debounce;
	u64 timestamp;
	int value;
};

/*
 * gpiod_get_from_child() - get gpio from node that is not a device itself
 * @node: pointer to the OF node
 * @prop_name: property name that node contains
 */
static struct gpio_desc *gpiod_get_from_of(struct device_node *node,
					   const char *prop_name,
					   enum gpiod_flags flags)
{
	struct gpio_desc *desc;
	enum of_gpio_flags of_flags;
	enum gpio_lookup_flags lookupflags = 0;
	int status;

	desc = of_get_named_gpiod_flags(node, prop_name, 0, &of_flags);
	if (IS_ERR(desc))
		return desc;

	if (of_flags & OF_GPIO_ACTIVE_LOW)
		lookupflags |= GPIO_ACTIVE_LOW;

	if (of_flags & OF_GPIO_SINGLE_ENDED) {
		if (of_flags & OF_GPIO_OPEN_DRAIN)
			lookupflags |= GPIO_OPEN_DRAIN;
		else
			lookupflags |= GPIO_OPEN_SOURCE;
	}

	status = gpiod_request(desc, prop_name);
	if (status < 0)
		return ERR_PTR(status);

	status = gpiod_configure_flags(desc, prop_name, lookupflags, flags);
	if (status < 0) {
		pr_info("gpio-hl: setup of GPIO %s failed\n", prop_name);
		gpiod_put(desc);
		return ERR_PTR(status);
	}

	return desc;
}

static void devm_gpiod_release(struct device *dev, void *res)
{
	struct gpio_desc **desc = res;

	gpiod_put(*desc);
}

/*
 * Modified gpiolib function for handling gpio request in this special case
 * @dev: device which manages resources
 * @node: OF node of one "real" GPIO line
 * @cond_id: name of line in OF
 */
static struct gpio_desc *devm_gpiod_get_from_of(struct device *dev,
						struct device_node *node,
						const char *con_id)
{
	struct gpio_desc **dr;
	struct gpio_desc *desc;

	dr = devres_alloc(devm_gpiod_release, sizeof(struct gpio_desc *),
			  GFP_KERNEL);
	if (!dr)
		return ERR_PTR(-ENOMEM);

	desc = gpiod_get_from_of(node, con_id, GPIOD_ASIS);
	if (IS_ERR(desc)) {
		devres_free(dr);
		return desc;
	}

	*dr = desc;
	devres_add(dev, dr);

	return desc;
}

/*
 * devm_of_get_vline - wrapper aroung devm_gpiod_get_from_of to request 3 pins
 * for vline
 * @dev: device which manages resources
 * @node: OF node of vline
 * @vl: virtual line to be assigned
 */
static int devm_of_get_vline(struct device *dev, struct device_node *node,
			     struct vline *vl)
{
	vl->high = devm_gpiod_get_from_of(dev, node, "high");
	if (IS_ERR(vl->high)) {
		dev_info(dev, "error claiming 'high' gpio");
		return PTR_ERR(vl->high);
	}
	vl->low = devm_gpiod_get_from_of(dev, node, "low");
	if (IS_ERR(vl->low)) {
		dev_info(dev, "error claiming 'low' gpio");
		return PTR_ERR(vl->low);
	}
	vl->in = devm_gpiod_get_from_of(dev, node, "input");
	if (IS_ERR(vl->in)) {
		dev_info(dev, "error claiming 'in' gpio");
		return PTR_ERR(vl->in);
	}
	return 0;
}

static int gpio_hl_set_vline_state(struct vline *vl, bool save)
{
	int hvalue = 0, lvalue = 0, ret = 0;
	int hout = gpiod_get_direction(vl->high);
	int lout = gpiod_get_direction(vl->low);

	if (save && hout == GPIOF_DIR_OUT && lout == GPIOF_DIR_OUT) {
		hvalue = gpiod_get_value_cansleep(vl->high);
		lvalue = gpiod_get_value_cansleep(vl->low);

		if (hvalue && lvalue) {
			pr_warn("gpio-hl: vline %s: high and low lines must"
				"not be set to 1 at the same time\n",
				vl->name);
			pr_warn("gpio-hl: vline %s: reseting both to 0"
				"setting vline as input\n",
				vl->name);
			hvalue = 0;
			lvalue = 0;
		}
		if (hvalue || lvalue)
			set_bit(VGPIO_IS_OUT, &vl->flags);
		if (hvalue)
			set_bit(VGPIO_VALUE, &vl->flags);
	}

	ret = gpiod_direction_input(vl->in);
	if (ret)
		return ret;
	ret = gpiod_direction_output(vl->low, lvalue);
	if (ret)
		return ret;
	ret = gpiod_direction_output(vl->high, hvalue);
	if (ret)
		return ret;

	return 0;
}

static int gpio_hl_dir_in(struct gpio_chip *chip, unsigned offset)
{
	struct gpio_vchip *vchip;
	struct vline *vl;

	vchip = get_vchip(chip);
	vl = vchip->lines + offset;

	gpiod_set_value_cansleep(vl->high, 0);
	gpiod_set_value_cansleep(vl->low, 0);

	clear_bit(VGPIO_IS_OUT, &vl->flags);

	return 0;
}

static int gpio_hl_set_cfg(struct gpio_chip *chip, unsigned offset,
			   unsigned long config)
{
	enum pin_config_param prm = pinconf_to_config_param(config);
	u32 arg = pinconf_to_config_argument(config);
	struct gpio_vchip *vchip;
	struct vline *vl;

	vchip = get_vchip(chip);
	vl = vchip->lines + offset;

	if (prm != PIN_CONFIG_INPUT_DEBOUNCE)
		return -ENOTSUPP;

	WRITE_ONCE(vl->in->debounce, arg);

	return 0;
}

static void gpio_hl_set(struct gpio_chip *chip, unsigned offset, int value)
{
	struct gpio_vchip *vchip;
	struct vline *vl;
	bool oldval;

	vchip = get_vchip(chip);
	vl = vchip->lines + offset;

	oldval = test_bit(VGPIO_VALUE, &vl->flags);

	if (oldval == !!value)
		return;

	if (value) {
		set_bit(VGPIO_VALUE, &vl->flags);
		gpiod_set_value_cansleep(vl->low, 0);
		gpiod_set_value_cansleep(vl->high, 1);
	} else {
		clear_bit(VGPIO_VALUE, &vl->flags);
		gpiod_set_value_cansleep(vl->high, 0);
		gpiod_set_value_cansleep(vl->low, 1);
	}
}

static int gpio_hl_get_dir(struct gpio_chip *chip, unsigned offset)
{
	struct gpio_vchip *vchip;
	struct vline *vl;

	vchip = get_vchip(chip);
	vl = vchip->lines + offset;

	return !test_bit(VGPIO_IS_OUT, &vl->flags);
}

static int gpio_hl_get(struct gpio_chip *chip, unsigned offset)
{
	struct gpio_vchip *vchip;
	struct vline *vl;

	vchip = get_vchip(chip);
	vl = vchip->lines + offset;

	if (test_bit(VGPIO_IS_OUT, &vl->flags)) {
		return test_bit(VGPIO_VALUE, &vl->flags);
	}

	return gpiod_get_value_cansleep(vl->in);
}

static int gpio_hl_dir_out(struct gpio_chip *chip, unsigned offset, int value)
{
	struct gpio_vchip *vchip;
	struct vline *vl;

	vchip = get_vchip(chip);
	vl = vchip->lines + offset;

	set_bit(VGPIO_IS_OUT, &vl->flags);

	/*
	 * Here is the little hack. To force gpio_hl_set() set the value,
	 * put inverse bit in a bitmap before call
	 */
	if (value)
		clear_bit(VGPIO_VALUE, &vl->flags);
	else
		set_bit(VGPIO_VALUE, &vl->flags);

	gpio_hl_set(chip, offset, value);

	return 0;
}

#ifdef CONFIG_DEBUG_FS
#include <linux/seq_file.h>
void gpio_hl_dbg_show(struct seq_file *s, struct gpio_chip *chip)
{
	int i = 0;
	int values[3];
	int gpios[3];
	int is_out;
	const char *label;
	struct vline *vl;
	struct gpio_vchip *vchip = get_vchip(chip);

	for (i = 0; i < chip->ngpio; i++) {
		label = gpiochip_is_requested(chip, i);
		if (!label)
			continue;

		vl = vchip->lines + i;
		values[0] = gpiod_get_value_cansleep(vl->high);
		values[1] = gpiod_get_value_cansleep(vl->low);
		values[2] = gpiod_get_value_cansleep(vl->in);
		gpios[0] = desc_to_gpio(vl->high);
		gpios[1] = desc_to_gpio(vl->low);
		gpios[2] = desc_to_gpio(vl->in);
		is_out = test_bit(VGPIO_IS_OUT, &vl->flags);

		seq_printf(
			s,
			" gpio-%-3d ( h-%-3d(%-1d) l-%-3d(%-1d) i-%-3d(%-1d)"
			" | %-10.10s ) %-3s %-3s\n",
			chip->base + i, gpios[0], values[0], gpios[1],
			values[1], gpios[2], values[2], label, 
			is_out ? "out" : "in",
			values[2] ? "hi" : "lo");
	}
	seq_printf(s, "\n");
}
#else
#define gpio_hl_dbg_show NULL
#endif

/*
 * find_vl_by_irq - function for finding vl line by irq
 * @vchip: virtual chip in which possibly holds needed line
 * @irq: irq number to compare with
 * @nested: flag denoting if we are searching vline by nested irq
 * @offset: pointer to value where to send offset, when NULL passed this
 * value ignored
 * @retval: return pointer to vl or -ENODEV in case of line not found
 */
static struct vline *find_vl_by_irq(struct gpio_vchip *vchip, int irq,
				    bool nested, int *offset)
{
	struct vline *it;
	int __offset = 0;

	for_each_vline(vchip, it)
	{
		int virq = nested ? irq_find_mapping(vchip->chip.irqdomain,
						     __offset) :
				    it->irq;
		if (virq == irq) {
			if (offset)
				*offset = __offset;
			return it;
		}
		__offset++;
	}

	dev_warn(vchip->chip.parent, "failed to found vline: %s", __func__);
	return ERR_PTR(-ENODEV);
}

static int irq_set_type(struct irq_data *d, unsigned int flow_type)
{
	struct gpio_chip *chip = irq_data_get_irq_chip_data(d);
	struct gpio_vchip *vchip = gpiochip_get_data(chip);
	struct vline *vl = find_vl_by_irq(vchip, d->irq, true, NULL);
	if (IS_ERR(vl))
		return PTR_ERR(vl);

	if (flow_type & (IRQ_TYPE_LEVEL_HIGH | IRQ_TYPE_LEVEL_LOW))
		return -EINVAL;

	WRITE_ONCE(vl->irqflags, flow_type);

	return 0;
}

static void irq_mask(struct irq_data *d)
{
	struct gpio_chip *chip = irq_data_get_irq_chip_data(d);
	struct gpio_vchip *vchip = gpiochip_get_data(chip);
	struct vline *vl = find_vl_by_irq(vchip, d->irq, true, NULL);

	if (IS_ERR(vl))
		return;

	WRITE_ONCE(vl->irq_unmasked, false);
}

static void irq_unmask(struct irq_data *d)
{
	struct gpio_chip *chip = irq_data_get_irq_chip_data(d);
	struct gpio_vchip *vchip = gpiochip_get_data(chip);
	struct vline *vl = find_vl_by_irq(vchip, d->irq, true, NULL);

	if (IS_ERR(vl))
		return;

	WRITE_ONCE(vl->irq_unmasked, true);
}

static struct irq_chip virqchip = {
	.name = "VGHL",
	.irq_mask = irq_mask,
	.irq_unmask = irq_unmask,
	.irq_set_type = irq_set_type,
};

/*
 * setup_nested_irqchip - setup virtual irqchip. Since it have multiple output
 * irq lines, default gpiolib function is not suitable, because it set a common
 * parent for all irqs.
 * @vchip: virtual chip, to connect irqchip with
 */
static int setup_nested_virqchip(struct gpio_vchip *vchip,
				 struct irq_chip *irqchip)
{
	struct gpio_chip *chip = &vchip->chip;
	struct vline *vl;
	int ret, offset;

	ret = gpiochip_irqchip_add_nested(&vchip->chip, irqchip, 0,
					  handle_level_irq, IRQ_TYPE_NONE);
	if (ret) {
		return ret;
	}

	if (!vchip->chip.irqdomain) {
		chip_err(chip, "called %s before setting up irqchip\n",
			 __func__);
		return -EINVAL;
	}

	/* Set corresponding parent to each irq */
	for (offset = 0; offset < vchip->lnum; offset++) {
		vl = vchip->lines + offset;
		irq_set_parent(irq_find_mapping(vchip->chip.irqdomain, offset),
			       vl->irq);
	}

	return 0;
}

#ifdef CONFIG_GPIO_HL_HOTPLUG
#include <linux/skbuff.h>
#include <linux/string.h>

static int hotplug_notify(struct event_data *data)
{
	struct sk_buff *skb = alloc_skb(1024, GFP_KERNEL);
	int ret = 0, len = 0;
	char buf[40];
	const char prefix[] = "HOME=/\0"
			      "PATH=/sbin:/bin:/usr/sbin:/usr/bin\0"
			      "SUBSYSTEM=io\0";

	if (!skb) {
		pr_err("Error allocation skb\n");
		ret = -ENOMEM;
		goto error;
	}
	memcpy(skb_put(skb, sizeof(prefix)), prefix, sizeof(prefix));
	len = snprintf(buf, 40, "GPIO=%s", data->name);
	memcpy(skb_put(skb, len + 1), buf, len + 1);
	len = snprintf(buf, 40, "TRIGGER=%s", data->trig);
	memcpy(skb_put(skb, len + 1), buf, len + 1);
	len = snprintf(buf, 40, "DEBOUNCE=%d", data->debounce);
	memcpy(skb_put(skb, len + 1), buf, len + 1);
	len = snprintf(buf, 40, "TIMESTAMP=%lld", data->timestamp);
	memcpy(skb_put(skb, len + 1), buf, len + 1);
	len = snprintf(buf, 40, "VALUE=%d", data->value);
	memcpy(skb_put(skb, len + 1), buf, len + 1);

	return broadcast_uevent(skb, 0, 1, GFP_KERNEL);
error:
	kfree_skb(skb);
	return ret;
}
#else
static int __maybe_unused hotplug_notify(struct event_data *data)
{
	return 0;
}
#endif /* CONFIG_GPIO_HL_HOTPLUG */

/*
 * gpio_hl_debounce_work_func - function launched after debouce period ended,
 * it gets the line value and timestamp. If not interrupt flags is set, when
 * simply returns, otherwise sends hotplug notify (which can be "null").
 */
static void gpio_hl_debounce_work_func(struct work_struct *work)
{
	int newval;
	u64 ts = 0;
	struct vline *vl = container_of(work, struct vline, dwork.work);
	struct event_data data = {
		.debounce = vl->in->debounce,
		.name = vl->name,
	};

	newval = gpiod_get_value_cansleep(vl->in);
	ts = ktime_to_ms(ktime_get_boottime());
	if (likely(!READ_ONCE(vl->hw_debounce)))
		ts -= vl->in->debounce;

	if (newval == vl->value)
		return;

	WRITE_ONCE(vl->value, newval);

	/* At least value changed, now check properly */
	if ((vl->irqflags & IRQ_TYPE_EDGE_BOTH) == IRQ_TYPE_EDGE_BOTH)
		data.trig = "BOTH";
	else if (vl->irqflags & IRQ_TYPE_EDGE_RISING && newval)
		data.trig = "RISE";
	else if (vl->irqflags & IRQ_TYPE_EDGE_FALLING && !newval)
		data.trig = "FALL";
	else
		return;

	data.value = newval;
	data.timestamp = ts;

	if (hotplug_notify(&data)) {
		pr_err("gpio-hl: error sending hotplug event\n");
	}
	return;
}

static irqreturn_t gpio_hl_irq_handler(int irq, void *p)
{
	int offset = 0, delay = 0;
	struct gpio_vchip *vchip = (struct gpio_vchip *)p;
	struct vline *vl = find_vl_by_irq(vchip, irq, false, &offset);

	if (unlikely(IS_ERR(vl)))
		return IRQ_NONE;

	if (!READ_ONCE(vl->irq_unmasked) || test_bit(VGPIO_IS_OUT, &vl->flags))
		return IRQ_NONE;

	handle_nested_irq(irq_find_mapping(vchip->chip.irqdomain, offset));

	delay = READ_ONCE(vl->hw_debounce) ?
			0 :
			msecs_to_jiffies(READ_ONCE(vl->in->debounce));

	mod_delayed_work(system_wq, &vl->dwork, delay);

	return IRQ_HANDLED;
}

/*
 * devm_gpio_hl_setup_vline_irq - sets up irq line state, at first it tries to
 * setup hardware debounce. If it fails then software debounce will be used.
 * Note: to save memory, in case of software debounce usage, debounce value
 * stored in vl->in descriptor.
 * @dev: device which manages resources
 * @vchip: virtual chip pointer passed to an irq handler
 * @vl: virtual line, where interrupt is setted up
 */
static int devm_gpio_hl_setup_vline_irq(struct device *dev,
					struct gpio_vchip *vchip,
					struct vline *vl)
{
	int ret = 0;

	vl->irq = gpiod_to_irq(vl->in);

	ret = devm_request_threaded_irq(dev, vl->irq, NULL, gpio_hl_irq_handler,
					IRQF_ONESHOT | IRQF_TRIGGER_RISING |
						IRQF_TRIGGER_FALLING,
					vl->name, vchip);
	if (ret) {
		dev_err(dev, "failed to setup interrupt\n");
		return ret;
	}

	ret = gpiod_set_debounce(vl->in, DEFAULT_DEBOUNCE);
	if (!ret) {
		dev_info(dev, "using hardware debounce on line %s", vl->name);
		/* Save the period */
		WRITE_ONCE(vl->hw_debounce, true);
		return ret;
	}
	WRITE_ONCE(vl->in->debounce, DEFAULT_DEBOUNCE);
	dev_dbg(dev, "using software debounce on line %s", vl->name);
	if (ret != -ENOTSUPP)
		return ret;

	INIT_DELAYED_WORK(&vl->dwork, gpio_hl_debounce_work_func);

	return 0;
}

static int gpio_hl_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct device_node *child;
	struct gpio_vchip *vchip;
	struct vline *vl;
	const char **names;
	int ret, offset;
	u32 lnum;
	bool save_state = false;

	save_state = of_property_read_bool(dev->of_node, "save-state");
	dev_info(dev, "save state %s\n", save_state ? "enabled" : "disabled");

	lnum = of_get_child_count(dev->of_node);
	if (!lnum) {
		dev_info(dev, "no vlines found for vchip\n");
		return -ENODEV;
	}

	vchip = devm_kzalloc(dev, sizeof(struct gpio_vchip), GFP_KERNEL);
	if (IS_ERR(vchip))
		return -ENOMEM;

	names = devm_kzalloc(dev, sizeof(char *) * lnum, GFP_KERNEL);
	if (IS_ERR(names))
		return -ENOMEM;

	vchip->lnum = lnum;
	vchip->lines =
		devm_kzalloc(dev, sizeof(struct vline) * lnum, GFP_KERNEL);
	if (IS_ERR(vchip->lines))
		return -ENOMEM;

	vchip->chip.label = "gpio-hl";
	vchip->chip.ngpio = lnum;
	vchip->chip.base = -1;
	vchip->chip.parent = dev;
	vchip->chip.can_sleep = true;
	vchip->chip.owner = THIS_MODULE;
	vchip->chip.get = gpio_hl_get;
	vchip->chip.set = gpio_hl_set;
	vchip->chip.of_node = dev->of_node;
	vchip->chip.direction_input = gpio_hl_dir_in;
	vchip->chip.direction_output = gpio_hl_dir_out;
	vchip->chip.set_config = gpio_hl_set_cfg;
	vchip->chip.get_direction = gpio_hl_get_dir;
	vchip->chip.dbg_show = gpio_hl_dbg_show;

	/* We need to setup names before _add_data */
	offset = 0;
	for_each_child_of_node(dev->of_node, child)
		names[offset++] = child->name;

	vchip->chip.names = names;

	ret = devm_gpiochip_add_data(dev, &vchip->chip, vchip);
	if (ret)
		return ret;

	/* Fill in default debounce values in descriptors */
	for (offset = 0; offset < lnum; offset++) {
		vchip->chip.gpiodev->descs[offset].debounce = DEFAULT_DEBOUNCE;
	}

	ret = setup_nested_virqchip(vchip, &virqchip);
	if (ret) {
		dev_err(dev, "error adding irqchip, to gpiochip\n");
		return ret;
	}

	offset = 0;
	for_each_child_of_node(dev->of_node, child) {
		vl = vchip->lines + offset;
		vl->name = child->name;
		ret = devm_of_get_vline(dev, child, vl);
		if (ret) {
			return ret;
		}

		ret = gpio_hl_set_vline_state(vl, save_state);
		if (ret) {
			dev_err(dev, "failed to init vline %s\n", vl->name);
			return ret;
		}

		ret = devm_gpio_hl_setup_vline_irq(dev, vchip, vl);
		if (ret) {
			dev_err(dev, "failed to setup irq on %s\n", vl->name);
			return ret;
		}

		dev_dbg(dev, "init line: %s irq: %d\n", child->name, vl->irq);
		offset++;
	}

	dev_info(dev, "registered %d gpios & irq handlers\n", lnum);

	return 0;
}

static const struct of_device_id gpio_hl_of_match[] = {
	{ .compatible = "gpio-hl" },
	{ /* sentinel */ },
};

static struct platform_driver gpio_hl_driver = {
	.probe = gpio_hl_probe,
	.driver = {
		.name = "gpio-hl",
		.owner = THIS_MODULE,
		.of_match_table = gpio_hl_of_match
	},
};

module_platform_driver(gpio_hl_driver);

MODULE_DEVICE_TABLE(of, gpio_hl_of_match);
MODULE_AUTHOR("Matthew Bystrin <mbystrin@radiofid.ru>");
MODULE_DESCRIPTION("IRZ gpio-hl driver");
MODULE_LICENSE("GPL");
