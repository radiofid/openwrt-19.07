/*
 * IRZ protection circuit control driver
 *
 * This driver was created to control protection circuits on IRZ devices.
 * Generic protection circuit architecture listed below:
 *
 *       .--------------.                          .--------------.
 *       |              |------(control lines)-----|              |
 *       |  Protection  |                          |              |
 *       |              |------(status lines)----- |    Driver    |
 *       |    circuit   |                          |              |
 *       |              |-----(interrupt lines)----|              |
 *       '--------------'                          '--------------'
 *
 *  Driver assume that every circuit have 3 types of lines:
 *  1. Control lines - lines that can change the protection circuit state;
 *  2. Status lines - lines that can tell the protection cirucuit state;
 *  3. Interrupt lines - lines that can tell about protection circuit state
 *  changes.
 *
 *  Note: status line can be also threated as interrupt , but not as control
 *  line.
 *
 *  Also driver do not know anything HOW to arm or disarm concrete cicuit, it's
 *  behaves like a proxy between userspace and hardware. All logic that impilies
 *  arming and disarming circuit must be implemented in userspace.
 */

#include <linux/bug.h>
#include <linux/cdev.h>
#include <linux/fs.h>
#include <linux/gpio/consumer.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/kernel.h>
#include <linux/kfifo.h>
#include <linux/ktime.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/platform_device.h>
#include <linux/poll.h>
#include <linux/spinlock.h>
#include <linux/uaccess.h>
#include <linux/wait.h>

#include <uapi/linux/protect.h>


#define PROTECT_MAXDEV 16
#define PROTECT_DEVNAME "protect"
#define PROTECT_CLASS "protect"
#define PROTECT_MAXEVENTS 16

static dev_t dev_num;
static struct class *protect_class;
static int offset = 0;

/*
 * struct protect_device - device for controlling protection cicuit
 * @pdev - pointer to correspondind platform device
 * @cdev - char dev representing device in userspace
 * @stat - array of status gpio descriptors
 * @ctrl - array of control gpio descriptors
 * @irqs - array of irq numbers
 * @irq_num - number of irqs
 * @seqnum - sequence number of last interrupt, incremented on every interrupt
 * @efifo - fifo for storing events
 * @lock - lock for fifi
 * @mtx - mutex for protecting ioctl call
 * @wq - wait queue for poll and read calls
 */
struct protect_device {
	struct platform_device *pdev;
	struct cdev cdev;

	struct gpio_descs *stat;
	struct gpio_descs *ctrl;
	int *irqs;
	int irq_num;

	u64 seqnum;

	DECLARE_KFIFO(efifo, struct protect_event, PROTECT_MAXEVENTS);
	spinlock_t lock;

	struct mutex mtx;
	struct wait_queue_head wq;
};


long protect_ioctl(struct file *file, unsigned int cmd, unsigned long arg)
{
	struct protect_device *prdev = file->private_data;
	struct device *dev = &prdev->pdev->dev;
	struct protect_req req;

	if (copy_from_user(&req, (struct protect_req *)arg,
			   sizeof(struct protect_req)))
		return -EFAULT;

	dev_info(dev, "ioctl call line: %d, val %d", req.ln, req.val);

	mutex_lock(&prdev->mtx);
	switch (cmd) {
	case PROTECT_SETCTRL:
		pr_info("protect set ctrl");
		if (req.ln >= prdev->ctrl->ndescs) {
			dev_err(dev, "invalid ctrl line number\n");
			return -EINVAL;
		}
		gpiod_set_value_cansleep(prdev->ctrl->desc[req.ln], !!req.val);
		break;
	case PROTECT_GETCTRL:
		pr_info("protect get ctrl");
		if (req.ln >= prdev->ctrl->ndescs) {
			dev_err(dev, "invalid ctrl line number\n");
			return -EINVAL;
		}
		req.val = gpiod_get_value_cansleep(prdev->ctrl->desc[req.ln]);
		break;
	case PROTECT_GETSTAT:
		pr_info("protect get stat");
		if (req.ln >= prdev->stat->ndescs) {
			dev_err(dev, "invalid stat line number\n");
			return -EINVAL;
		}
		req.val = gpiod_get_value_cansleep(prdev->stat->desc[req.ln]);
		break;
	default:
		mutex_unlock(&prdev->mtx);
		return -ENOTTY;
	}
	mutex_unlock(&prdev->mtx);

	return copy_to_user((struct protect_req *)arg, &req,
			    sizeof(struct protect_req));
}

static unsigned int protect_poll(struct file *file, poll_table *wait)
{
	struct protect_device *prdev = file->private_data;
	unsigned int mask = 0;
	pr_info("protect: poll\n");

	poll_wait(file, &prdev->wq, wait);

	spin_lock(&prdev->lock);
	if (!kfifo_is_empty(&prdev->efifo))
		mask |= (POLLIN | POLLRDNORM);
	spin_unlock(&prdev->lock);

	return mask;
}

static ssize_t protect_read(struct file *file, char __user *buf, size_t len,
			    loff_t *pos)
{
	struct protect_device *prdev = file->private_data;
	struct protect_event event;
	int ret = -1;
	pr_info("reading from protect device %s\n", prdev->pdev->name);

	if (len < sizeof(struct protect_event)) {
		return -EINVAL;
	}

	while(true) {
		spin_lock(&prdev->lock);
		ret = !kfifo_is_empty(&prdev->efifo);
		spin_unlock(&prdev->lock);

		if (ret)
			break;

		if (file->f_flags & O_NONBLOCK)
			return -EAGAIN;

		ret = wait_event_interruptible(prdev->wq,
						!kfifo_is_empty(&prdev->efifo));

		if (ret == -ERESTARTSYS)
			return ret; 
	}

	kfifo_get(&prdev->efifo, &event);

	ret = copy_to_user(buf, &event, sizeof(struct protect_event));
	if (ret)
		return -EFAULT;

	return sizeof(struct protect_event);
}

static int protect_open(struct inode *inode, struct file *file)
{
	struct protect_device *prdev = container_of(inode->i_cdev, 
						    struct protect_device,
						    cdev);
	file->private_data = prdev;
	pr_info("opening protect device %s\n", prdev->pdev->name);
	return 0;
}

static struct file_operations protect_fops = {
	.llseek			= no_llseek,
	.read			= protect_read,
	.open			= protect_open,
	.poll			= protect_poll,
	.unlocked_ioctl		= protect_ioctl,
};

/*
 * find_line_by_irq - finds line number where interrupt fired
 * @irq - irq number
 * @prdev - protect device 
 */
static int find_line_by_irq(int irq, struct protect_device *prdev)
{
	int i;
	for (i = 0; i < prdev->irq_num; i++) {
		if (prdev->irqs[i] == irq)
			return i;
	}
	WARN(true, "line not found");
	return -1;
}

static irqreturn_t protect_irq_handler(int irq, void *arg)
{
	struct protect_device *prdev = arg;
	struct protect_event event;
	unsigned long flags;

	event.ln = find_line_by_irq(irq, prdev);
	event.ts = ktime_to_us(ktime_get_boottime());

	pr_info("protect interrupt %lld %d\n", event.ts, event.ln);

	spin_lock_irqsave(&prdev->lock, flags);
	event.sn = prdev->seqnum++;
	if (kfifo_is_full(&prdev->efifo))
		kfifo_skip(&prdev->efifo);
	kfifo_put(&prdev->efifo, event);
	spin_unlock_irqrestore(&prdev->lock, flags);

	wake_up_interruptible(&prdev->wq);

	return IRQ_HANDLED;
}

/*
 * protect_setup_irq - setup irq handlers for all irqs listed in dt
 * @prdev - platform device to operate with
 */
static int protect_setup_irq(struct protect_device *prdev)
{
	struct platform_device *pdev = prdev->pdev;
	struct device *dev = &prdev->pdev->dev;
	int irq_num;
	int ret, i;

	irq_num = platform_irq_count(pdev);
	if (irq_num < 0)
		return irq_num;

	prdev->irq_num = irq_num;
	prdev->irqs = devm_kzalloc(dev, irq_num * sizeof(int), GFP_KERNEL);

	for (i = 0; i < irq_num; i++) {
		prdev->irqs[i] = platform_get_irq(pdev, i);
		ret = devm_request_any_context_irq(dev, prdev->irqs[i],
						   protect_irq_handler, 0,
						   "protect", prdev);
		if (ret < 0) {
			dev_err(dev, "failed to request irq\n");
			return ret;
		}
	}

	dev_info(dev, "setup %d irq handlers\n", prdev->irq_num);

	return 0;
}

/*
 * protect_setup_device - create character device in devfs
 * @prdev - protect device
 */
static int protect_setup_device(struct protect_device *prdev)
{
	struct device *dev = &prdev->pdev->dev;
	const char *name = prdev->pdev->name;
	struct device *dev_ret;
	dev_t curr;
	int ret = -1;

	cdev_init(&prdev->cdev, &protect_fops);
	prdev->cdev.owner = THIS_MODULE;

	curr = MKDEV(MAJOR(dev_num), MINOR(dev_num) + offset);

	ret = cdev_add(&prdev->cdev, curr, 1);
	if (ret) {
		dev_err(dev, "failed to add cdev\n");
		return ret;
	}

	dev_ret = device_create(protect_class, NULL, curr, NULL, name);
	if (IS_ERR(dev_ret)) {
		dev_err(dev, "failed to create device\n");
		cdev_del(&prdev->cdev);
		return PTR_ERR(dev_ret);
	}

	offset++;
	return 0;
}

/*
 * protect_setup_lines - claim gpiolines listed in dt and init it
 * @prdev - protect device
 */
static int protect_setup_lines(struct protect_device *prdev)
{
	struct device *dev = &prdev->pdev->dev;

	prdev->stat = devm_gpiod_get_array(dev, "stat", GPIOD_IN);
	if (IS_ERR(prdev->stat)) {
		dev_warn(dev, "failed to get stat gpios from dt\n");
	}

	prdev->ctrl = devm_gpiod_get_array(dev, "ctrl", GPIOD_OUT_LOW);
	if (IS_ERR(prdev->ctrl)) {
		dev_err(dev, "failed to get ctrl gpios from dt\n");
		return -ENODEV;
	}

	return 0;
}

static int protect_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	struct protect_device *prdev;
	int ret = -1;

	prdev = devm_kzalloc(dev, sizeof(struct protect_device), GFP_KERNEL);
	if (IS_ERR(prdev)) {
		dev_err(dev, "failed to allocate memory for device\n");
		return -ENOMEM;
	}

	INIT_KFIFO(prdev->efifo);
	spin_lock_init(&prdev->lock);
	mutex_init(&prdev->mtx);
	init_waitqueue_head(&prdev->wq);
	prdev->pdev = pdev;

	ret = protect_setup_lines(prdev);
	if (ret) {
		dev_err(dev, "failed to setup lines\n");
		return ret;
	}

	ret = protect_setup_irq(prdev);
	if (ret) {
		dev_err(dev, "failed to setup irq\n");
		return ret;
	}

	ret = protect_setup_device(prdev);
	if (ret) {
		dev_err(dev, "failed to setup device\n");
		return ret;
	}

	platform_set_drvdata(pdev, prdev);
	return 0;
}

static const struct of_device_id protect_of_match[] = {
	{ .compatible = "protect" },
	{ /* sentinel */ },
};

static struct platform_driver protect_driver = {
	.probe = protect_probe,
	.driver = {
		.name = "protect",
		.owner = THIS_MODULE,
		.of_match_table = protect_of_match
	},
};

static int __init protect_init(void)
{
	int ret = -1;

	ret = alloc_chrdev_region(&dev_num, 0, PROTECT_MAXDEV, PROTECT_DEVNAME);
	if (ret) {
		pr_err("protect: failed to allocate cdev region\n");
		goto chrdev_err;
	}

	protect_class = class_create(THIS_MODULE, PROTECT_CLASS);
	if (IS_ERR(protect_class)) {
		pr_err("protect: failed to create class\n");
		goto class_err;
	}

	ret = platform_driver_register(&protect_driver);
	if (ret) {
		pr_err("protect: failed to register driver\n");
		goto driver_err;
	}

	return 0;

driver_err:
	class_destroy(protect_class);
class_err:
	unregister_chrdev_region(dev_num, PROTECT_MAXDEV);
chrdev_err:
	return ret;
};

module_init(protect_init);

MODULE_DEVICE_TABLE(of, protect_of_match);
MODULE_AUTHOR("Matthew Bystrin <mbystrin@radiofid.ru>");
MODULE_DESCRIPTION("Generic IRZ driver for protection circuits");
MODULE_LICENSE("GPL");
