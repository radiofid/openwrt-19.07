#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/reboot.h>
#include <linux/delay.h>
#include <linux/of_platform.h>
#include <linux/gpio/consumer.h>
#include <linux/platform_device.h>

static struct gpio_desc *rst = NULL;

static int irz_reset(struct notifier_block *nb, unsigned long mode, void *cmd)
{
	/* Need to put a delay to see the mesasge*/
	pr_info("irz-reset: performing reset\n");
	mdelay(1000);
	gpiod_set_value_cansleep(rst, 1);
	gpiod_put(rst);
	return NOTIFY_DONE;
}

static struct notifier_block irz_reset_nb = {
	.notifier_call = irz_reset,
	.priority = 255, /* Have to start before default handler */
};

/* 
 * Setup a restart handler when device match occures. If in DT more than one
 * device compatible with "irz-reset" raises an error.
 */
static int irz_reset_probe(struct platform_device *pdev)
{
	struct device *dev = &pdev->dev;
	if (rst) {
		pr_warn("irz-reset: more than one reset pin in system."
			"check your config\n");
		return -EPERM;
	}
	rst = gpiod_get(dev, "reset", GPIOD_OUT_LOW); 
	if (IS_ERR(rst)) {
		pr_warn("irz-reset: reset pin not found."
			"hardware reset disabled\n");
		return -ENODEV;
	}
	pr_info("irz-reset: found reset pin\n");
	register_reboot_notifier(&irz_reset_nb);
	register_restart_handler(&irz_reset_nb);
	return 0;
}

static struct of_device_id of_ids[] = {
	{ .compatible = "irz-reset" },
	{ /* sentinel */}
};

static struct platform_driver irz_reset_driver = {
	.probe = irz_reset_probe,
	.driver = {
		.name = "irz-reset",
		.owner = THIS_MODULE,
		.of_match_table = of_ids
	}
};
module_platform_driver(irz_reset_driver);

MODULE_AUTHOR("Matthew Bystrin <mbystrin@radiofid.ru>");
MODULE_DESCRIPTION("IRZ reset driver");
MODULE_DEVICE_TABLE(of, of_ids);
MODULE_LICENSE("GPL");
