/dts-v1/;

/include/ "mt7620a.dtsi"

/ {
	compatible = "ralink,irz_mt02", "ralink,mt7620a-soc";
	model = "R2 board";

	aliases {
		led-boot = &STATUS;
		led-failsafe = &STATUS;
		led-running = &STATUS;
		led-upgrade = &STATUS;
	};
	chosen {
	    bootargs = "";
	};
	palmbus@10000000 {
		i2c@900 {
			status = "okay";
			reset-mt7620;
			rtc@51 {
				compatible = "nxp,pcf85063";
				reg = <0x51>;
			};
			gps@42 {
				compatible = "ublox,neo";
				reg = <0x42>;
			};
			exp: exp@20 {
				#gpio-cells = <2>;
				compatible = "sx1503q";
				reg = <0x20>;
				gpio-controller;
				int = <&gpio0 22 0>;
			};
		};

		spi@b00 {
			status = "okay";

			m25p80@0 {
				#address-cells = <1>;
				#size-cells = <1>;
				compatible = "w25q128";
				reg = <0 0>;
				linux,modalias = "m25p80", "w25q128", "mx66l51235l";
				spi-max-frequency = <50000000>;

				partition@0 {
					label = "u-boot";
					reg = <0x0 0x30000>;
				};

				partition@30000 {
					label = "u-boot-env";
					reg = <0x30000 0x10000>;
				};

				factory: partition@40000 {
					label = "factory";
					reg = <0x40000 0x10000>;
				};
				partition@50000 {
					label = "firmware";
					reg = <0x50000 0xbb0000>;
				};
				partition {
					label = "opt";
					reg = <0xc00000 0x400000>;
				};
			};
		};



		uart@500 {
		    status = "okay";
		    pinctrl-names = "default";
			pinctrl-0 = <&uartf_pins>;
		};
	    	gpio@600 {
			status = "okay";
		};
		gpio@638 {
			status = "okay";
		};
		gpio@660 {
			status = "okay";
		};
		gpio@688 {
			status = "okay";
		};
	};

	pinctrl {
		state_default: pinctrl0 {
			gpio {
				ralink,group = "nd_gpio", "rgmii1", "rgmii2", "wled", "wdt", "mdio";
				ralink,function = "gpio";
			};
		};
		uartf_pins: uartf {
			uartf {
				ralink,group = "uartf";
				ralink,function = "uartf";
			};
		};
	};

	ethernet@10100000 {
		status = "okay";

		pinctrl-names = "default";
		//pinctrl-0 = <&mdio_pins>;

		ralink,port-map = "llll";
		mtd-mac-address = <&factory 0x28>;

		port@4 {
			status = "okay";
			phy-mode = "ephy";
			phy-handle = <&phy4>;
		};

		mdio-bus {
			status = "okay";

			phy4: ethernet-phy@4 {
				reg = <4>;
				phy-mode = "ephy";
			};
		};
	};



	gsw@10110000 {
		ralink,port4 = "ephy";
	};

	pcie@10140000 {
		status = "okay";
	};

	spi_gpio {
	    compatible = "spi-gpio";
	    #address-cells = <0x1>;
	    ranges;
	    gpio-sck  = <&gpio2 22 0>;
	    gpio-mosi = <&gpio2 23 0>;
	    cs-gpios  = <&gpio2 21 0>;
	    num-chipselects = <1>;


		gpiosh: gpio_shift@0 {
			compatible = "fairchild,74hc595";
			reg = <0>;
			gpio-controller;
			#gpio-cells = <2>;
			registers-number = <2>;
			spi-max-frequency = <10000>;
		};
	};

	gpio_tr: gpio-tr@0 {
	    compatible = "gpio-tr";
	    #address-cells = <1>;
	    #gpio-cells = <2>;
	    gpio-controller;
	    IO_7 {
		low_pin		= <&gpiosh 1  0x0>;
		high_pin	= <&gpiosh 9  0x0>;
		input_pin	= <&gpio2  31 0x1>;
	    };
	    IO_6 {
		low_pin		= <&gpiosh 2  0x0>;
		high_pin	= <&gpiosh 10 0x0>;
		input_pin	= <&gpio2  30 0x1>;
	    };
	    IO_5 {
		low_pin		= <&gpiosh 3  0x0>;
		high_pin	= <&gpiosh 11 0x0>;
		input_pin	= <&gpio2  26 0x1>;
	    };
	    IO_4 {
		low_pin		= <&gpiosh 4  0x0>;
		high_pin	= <&gpiosh 12 0x0>;
		input_pin	= <&gpio2  27 0x1>;
	    };
	    IO_3 {
		low_pin		= <&gpiosh 5  0x0>;
		high_pin	= <&gpiosh 13 0x0>;
		input_pin	= <&gpio2  28 0x1>;
	    };
	    IO_2 {
		low_pin		= <&gpiosh 6  0x0>;
		high_pin	= <&gpiosh 14 0x0>;
		input_pin	= <&gpio2  29 0x1>;
	    };
	    IO_1 {
		low_pin		= <&gpiosh 7  0x0>;
		high_pin	= <&gpiosh 15 0x0>;
		input_pin	= <&gpio2  25 0x1>;
	    };
	};

	pps {
		compatible = "pps-gpio";
		gpios = <&gpio1 8 0>;
		assert-falling-edge;
	};
	gpio {
		compatible = "gpio-export";
		GSM_POWER {
		    gpios = <&gpio0 0 1>;
		    gpio-export,name  = "gsm:power";
		    gpio-export,output = <0>;
		};
		GSM2_RESET {
			gpios = <&gpio0 23 0>;
			gpio-export,name = "gsm2:reset";
			gpio-export,output = <0>;
		};
		GSM2_POWER {
		    gpios = <&gpio1 6 1>;
		    gpio-export,name  = "gsm2:power";
		    gpio-export,output = <1>;
		};
		GPS_RESET {
		    gpios = <&gpio1 9 1>;
		    gpio-export,name  = "gps:reset";
		    gpio-export,output = <1>;
		};
		RS232_ENABLE {
		    gpios = <&gpio1 7 0>;
		    gpio-export,name  = "rs232:enable";
		    gpio-export,output = <1>;
		};
		RS485_ENABLE {
		    gpios = <&gpio1 11 1>;
		    gpio-export,name  = "rs485:enable";
		    gpio-export,output = <1>;
		};
		sim_switch {
		    gpios = <&gpio1 3 0>;
		    gpio-export,name  = "sim:switch";
		    gpio-export,output = <1>;
		};
//		OPTO_IN {
//		    gpios = <&gpio1 10 1>;
//		    gpio-export,name  = "optical";
//		    gpio-export,input;
//		};
		GSM2_ENABLE {
		    gpios = <&gpio2 5 0 >;
		    gpio-export,name  = "gsm2:enable";
		    gpio-export,output = <0>;
		};
	};
	gpio-keys-polled {
		compatible = "gpio-keys-polled";
		#address-cells = <1>;
		#size-cells = <0>;
		poll-interval = <20>;
		reset {
			label = "reset";
			gpios = <&gpio0 17 1>;
			linux,code = <0x198>;
		};
	};
	gpio-leds {
		compatible = "gpio-leds";
		STATUS: power {
			label = "status";
			gpios = <&gpio1 5 1>;
		};
		CONNECT_GREEN {
			label = "connect:green";
			gpios = <&gpio1 4 0>;
		};
		CONNECT_RED {
			label = "connect:red";
			gpios = <&gpio1 0 0>;
		};
		SIGNAL_GREEN {
			label = "signal:green";
			gpios = <&gpio1 1 0>;
		};
		SIGNAL_RED {
			label = "signal:red";
			gpios = <&gpio1 2 0>;
		};
		WLAN {
			label = "wlan";
			gpios = <&gpio3 0 1>;
		};
	};
	gpio-reset {
		compatible = "irz-reset";
		reset-gpios = <&gpio2 24 0>;
	};
	ehci@101c0000 {
		status = "okay";
	};

	ohci@101c1000 {
		status = "okay";
	};
	wmac@10180000 {
		status = "okay";
		ralink,mtd-eeprom = <&factory 0>;
	};
	sdhci@10130000 {
		status = "okay";

		pinctrl-names = "default";
		pinctrl-0 = <&sdhci_pins>;
	};

};

