#!/bin/sh

. /lib/functions/system.sh
. /lib/functions.sh

platform_check_image() {
    local board=$(board_name)

    case "$board" in
        "irz_kw04"|"irz_kw44")
            nand_do_platform_check $board $1
            return $?
            ;;
    esac
    echo "Sysupgrade is not yet supported on $board"
    return 1
}

platform_do_upgrade() {
	local board="$(board_name)"

	case "$board" in
	*)
		nand_do_upgrade "$1"
		;;
	esac
}
