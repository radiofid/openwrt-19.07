#!/bin/sh
#
# Copyright (C) 2011-2012 OpenWrt.org
#

ubootenv_add_uci_config() {
	local dev=$1
	local offset=$2
	local envsize=$3
	local secsize=$4
	local numsec=$5
	uci batch <<EOF
add ubootenv ubootenv
set ubootenv.@ubootenv[-1].dev='$dev'
set ubootenv.@ubootenv[-1].offset='$offset'
set ubootenv.@ubootenv[-1].envsize='$envsize'
set ubootenv.@ubootenv[-1].secsize='$secsize'
set ubootenv.@ubootenv[-1].numsec='$numsec'
EOF
	uci commit ubootenv
}

ubootenv_add_app_config() {
	local dev
	local offset
	local envsize
	local secsize
	local numsec
	config_get dev "$1" dev
	config_get offset "$1" offset
	config_get envsize "$1" envsize
	config_get secsize "$1" secsize
	config_get numsec "$1" numsec
	echo "$dev $offset $envsize $secsize $numsec" >>/etc/fw_env.config
}

ubootenv_get_var(){
	if [ -f /etc/fw_env.config ]; then
		fw_printenv -n "$1" 2>/dev/null
		return $?
	elif [ -f /tmp/fw_env.config ]; then
		fw_printenv -c /tmp/fw_env.config -n "$1" 2>/dev/null
		return $?
	else
		echo "get_var_from_env: no config found">&2
		return 1
	fi
}
