#include <linux/gpio.h>
#include <linux/i2c.h>
#include <linux/init.h>
#include <linux/interrupt.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/slab.h>
#include <linux/workqueue.h>
#include <linux/delay.h>
#include <linux/of_irq.h>
#include <linux/of_gpio.h>
#include <linux/skbuff.h>

#define NO_UPDATE_PENDING	-1

#define SX1503X_DIRA_DEFAULT        0x24
#define SX1503X_DIRB_DEFAULT        0x09
#define SX1503X_DATAA_DEFAULT       0xB6
#define SX1503X_DATAB_DEFAULT       0x0D

#define SX1503X_INPUT_GPIO          1
#define SX1503X_OUTPUT_GPIO         0

#define SX1503X_OUTPUT_GPIO_1       0
#define SX1503X_OUTPUT_GPIO_2       3
#define SX1503X_OUTPUT_GPIO_3       6
#define SX1503X_OUTPUT_GPIO_4       9
#define SX1503X_INPUT_GPIO_1        2
#define SX1503X_INPUT_GPIO_2        5
#define SX1503X_INPUT_GPIO_3        8
#define SX1503X_INPUT_GPIO_4        11
#define SX1503X_ENABLE_OUTPUT       15

#define SX1503_DEFAULT_DEBOUNCE     100
#define SX1503_MIN_DEBOUNCE         50

struct sx1503_device_data {
	u8 reg_pullup;
	u8 reg_pulldn;
	u8 reg_dir;
	u8 reg_data;
	u8 reg_sense;
	u8 reg_irq_mask;
	u8 reg_irq_src;
	u8 ngpios;
};

struct sx1503_chip {
	struct gpio_chip                 gpio_chip;
	struct i2c_client               *client;
	const struct sx1503_device_data *dev_cfg;
	struct delayed_work              dwork;
	u32                              dwork_timeout;
	int                              irq_gpio;
	int                              irq_base;
	int                              irq_update;
	u32                              irq_sense;
	u32                              irq_masked;
	u32                              dev_sense;
	u32                              dev_masked;
	u32                              debounce[16];
	u32                              timestamp[16];
	u32                              old_time;
	struct irq_chip                  irq_chip;
	struct mutex                     lock;
};

static const struct sx1503_device_data sx1503_devices = {
	.reg_pullup	= 0x05,
	.reg_pulldn	= 0x07,
	.reg_dir	= 0x03,
	.reg_data	= 0x01,
	.reg_sense	= 0x0D,
	.reg_irq_mask = 0x09,
	.reg_irq_src = 0x0F,
	.ngpios = 9,
};

static const struct i2c_device_id sx1503_id[] = {
	{"sx1503q", 0},
	{},
};
MODULE_DEVICE_TABLE(i2c, sx1503_id);

static const char* const sx1503_gpio_names[] = {
	"GPO_4",
	"GPI_4",
	"GPO_3",
	"GPI_3",
	"GPO_2",
	"GPI_2",
	"GPO_1",
	"GPI_1",
	"ENABLE_SX1503",
};


struct gpio_event {
	struct gpio_chip*       chip;
	char*                   gpio;
	int                     action;
	int                     debounce;
	u64                     timestamp;
	struct work_struct      work;
	struct sk_buff*         skb;
};


static int gpio_sx_add_var(struct gpio_event* event, int argv, const char* format, ...)
{
	static char buf[128];
	char* s;
	va_list args;
	int len;
	if (argv)
		return 0;

	va_start(args, format);
	len = vsnprintf(buf, sizeof(buf), format, args);
	va_end(args);

	if (len >= sizeof(buf)) {
		return -ENOMEM;
	}

	s = skb_put(event->skb, len + 1);
	strcpy(s, buf);
	return 0;
}


static void gpio_sx_event_work(struct work_struct* ws)
{
	struct gpio_event* ge;
	int ret = 0;

	ge = container_of(ws, struct gpio_event, work);

	ge->skb = alloc_skb(2048, GFP_KERNEL);
	if (!ge->skb)
		goto free_ge;
	
	ret = gpio_sx_add_var(ge, 0, "HOME=%s","/");
	if(ret)
		goto free_skb;
	
	ret = gpio_sx_add_var(ge, 0, "PATH=%s","/sbin:/bin:/usr/sbin:/usr/bin");
	if(ret)
		goto free_skb;

	ret = gpio_sx_add_var(ge, 0, "SUBSYSTEM=%s", "io");
	if(ret)
		goto free_skb;

	ret = gpio_sx_add_var(ge, 0, "GPIO=%s", ge->gpio);
	if(ret)
		goto free_skb;

	ret = gpio_sx_add_var(ge, 0, "TRIGGER=%s", (ge->action == 2) ? "RISE": (ge->action == 1) ? "FALL" : "BOTH");
	if(ret)
		goto free_skb;

	ret = gpio_sx_add_var(ge, 0, "DEBOUNCE=%i", ge->debounce);
	if (ret)
		goto free_skb;

	ret = gpio_sx_add_var(ge, 0, "TIMESTAMP=%lli", ge->timestamp);
	if (ret)
		goto free_skb;

	broadcast_uevent(ge->skb, 0, 1, GFP_KERNEL);

free_skb:
	if(ret) {
		kfree_skb(ge->skb);
	}

free_ge:
	kfree(ge);
}


int gpio_sx_create_event(struct gpio_chip* chip, char* const gpio, int action, unsigned debounce, unsigned timestamp)
{
	struct gpio_event* ge;
	ge = kzalloc(sizeof(struct gpio_event), GFP_KERNEL);
	if (!ge)
		return -ENOMEM;

	ge->chip = chip;
	ge->gpio = gpio;
	ge->action = action;
	ge->debounce = debounce;
	ge->timestamp = timestamp;
	INIT_WORK(&ge->work, (void*)gpio_sx_event_work);
	schedule_work(&ge->work);
	return 0;
}


static s32 sx1503_i2c_write(struct i2c_client *client, u8 reg, u8 val)
{
	s32 err = i2c_smbus_write_byte_data(client, reg, val);

	if (err < 0)
		dev_warn(&client->dev,
			"i2c write fail: can't write %02x to %02x: %d\n",
			val, reg, err);
	return err;
}

static s32 sx1503_i2c_read(struct i2c_client *client, u8 reg, u8 *val)
{
	s32 err = i2c_smbus_read_byte_data(client, reg);

	if (err >= 0)
		*val = err;
	else
		dev_warn(&client->dev,
			"i2c read fail: can't read from %02x: %d\n",
			reg, err);
	return err;
}

/*
 * REGISTER N-1 [ f e d c b a 9 8 ]
 *          N   [ 7 6 5 4 3 2 1 0 ]
 */
static inline void sx1503_find_cfg(u8 offset, u8 width,
				u8 *reg, u8 *mask, u8 *shift)
{
	*reg   -= offset * width / 8;
	*mask   = (1 << width) - 1;
	*shift  = (offset * width) % 8;
	*mask <<= *shift;
}

static s32 sx1503_write_cfg(struct sx1503_chip *chip,
			u8 offset, u8 width, u8 reg, u8 val)
{
	u8  mask;
	u8  data;
	u8  shift;
	s32 err;

	sx1503_find_cfg(offset, width, &reg, &mask, &shift);
	// TODO:
	switch(reg) {
		case 0x0C: reg = 0xB; break;
		case 0x0B: reg = 0xC; break;
		default: break;
	}
	err = sx1503_i2c_read(chip->client, reg, &data);
	if (err < 0)
		return err;

	data &= ~mask;
	data |= (val << shift) & mask;
	return sx1503_i2c_write(chip->client, reg, data);
}

static int sx1503_get_io(struct sx1503_chip *chip, unsigned offset)
{
	u8  reg = chip->dev_cfg->reg_data;
	u8  mask;
	u8  data;
	u8  shift;
	s32 err;

	sx1503_find_cfg(offset, 1, &reg, &mask, &shift);
	err = sx1503_i2c_read(chip->client, reg, &data);
	if (err >= 0)
		err = (data & mask) != 0 ? 1 : 0;

	return err;
}

static void sx1503_set_io(struct sx1503_chip *chip, unsigned offset, int val)
{
	sx1503_write_cfg(chip,
			offset,
			1,
			chip->dev_cfg->reg_data,
			(val ? 1 : 0));
}

static int sx1503_io_input(struct sx1503_chip *chip, unsigned offset)
{
	return sx1503_write_cfg(chip,
				offset,
				1,
				chip->dev_cfg->reg_dir,
				1);
}

static int sx1503_io_output(struct sx1503_chip *chip, unsigned offset, int val)
{
	int err;

	err = sx1503_write_cfg(chip,
			offset,
			1,
			chip->dev_cfg->reg_data,
			(val ? 1 : 0));
	if (err >= 0)
		err = sx1503_write_cfg(chip,
				offset,
				1,
				chip->dev_cfg->reg_dir,
				0);
	return err;
}

static struct sx1503_gpio_s {
	int input;
	int gpio;
};

static struct sx1503_gpio_s sx1503_gpio[] = {
	{ SX1503X_OUTPUT_GPIO, SX1503X_OUTPUT_GPIO_1 },
	{ SX1503X_INPUT_GPIO,  SX1503X_INPUT_GPIO_1 },
	{ SX1503X_OUTPUT_GPIO, SX1503X_OUTPUT_GPIO_2 },
	{ SX1503X_INPUT_GPIO,  SX1503X_INPUT_GPIO_2 },
	{ SX1503X_OUTPUT_GPIO, SX1503X_OUTPUT_GPIO_3 },
	{ SX1503X_INPUT_GPIO,  SX1503X_INPUT_GPIO_3 },
	{ SX1503X_OUTPUT_GPIO, SX1503X_OUTPUT_GPIO_4 },
	{ SX1503X_INPUT_GPIO,  SX1503X_INPUT_GPIO_4 },
	{ SX1503X_OUTPUT_GPIO, SX1503X_ENABLE_OUTPUT }
};


static int sx1503_gpio_get(struct gpio_chip *gc, unsigned offset)
{
	struct sx1503_chip *chip;
	int status = -EINVAL;
	int status_of = -EINVAL;

	chip = container_of(gc, struct sx1503_chip, gpio_chip);

	mutex_lock(&chip->lock);

	status = sx1503_get_io(chip, sx1503_gpio[offset].gpio);
	if (!sx1503_gpio[offset].input && sx1503_gpio[offset].gpio != SX1503X_ENABLE_OUTPUT) {
		status_of = sx1503_get_io(chip, sx1503_gpio[offset].gpio + 1);
		if (status_of != status)
			status = 0;
	}
	if (sx1503_gpio[offset].input) {
		status = !status;
	}
	mutex_unlock(&chip->lock);
	return status;
}


static void sx1503_gpio_set(struct gpio_chip *gc, unsigned offset, int val)
{
	struct sx1503_chip *chip;

	chip = container_of(gc, struct sx1503_chip, gpio_chip);

	mutex_lock(&chip->lock);

	sx1503_set_io(chip, sx1503_gpio[offset].gpio, val);
	if (!sx1503_gpio[offset].input) {
		sx1503_set_io(chip, sx1503_gpio[offset].gpio + 1, val);
	}

	mutex_unlock(&chip->lock);
}


static int sx1503_gpio_direction_input(struct gpio_chip *gc, unsigned offset)
{
	struct sx1503_chip *chip;
	int status = -EINVAL;
	int status_of = -EINVAL;

	chip = container_of(gc, struct sx1503_chip, gpio_chip);

	mutex_lock(&chip->lock);

	status = sx1503_io_input(chip, sx1503_gpio[offset].gpio);
	if (!sx1503_gpio[offset].input) {
		status_of = sx1503_io_input(chip, sx1503_gpio[offset].gpio + 1);
		if (status_of != status)
			status = 0;
	}

	mutex_unlock(&chip->lock);
	return status;
}


static int sx1503_gpio_direction_output(struct gpio_chip *gc,
					unsigned offset,
					int val)
{
	struct sx1503_chip *chip;
	int status = 0;
	int status_of = 0;

	chip = container_of(gc, struct sx1503_chip, gpio_chip);

	mutex_lock(&chip->lock);
	status = sx1503_io_output(chip, sx1503_gpio[offset].gpio, val);
	if (!sx1503_gpio[offset].input) {
		status_of = sx1503_io_output(chip, sx1503_gpio[offset].gpio + 1, val);
		if (status_of != status)
			status = 0;
	}

	mutex_unlock(&chip->lock);
	return status;
}


static int sx1503_gpio_set_config(struct gpio_chip *gc,
        unsigned offset, unsigned long config)
{
	struct sx1503_chip* chip = container_of(gc, struct sx1503_chip, gpio_chip);
	unsigned long param = pinconf_to_config_param(config);
	unsigned arg = pinconf_to_config_argument(config);
	unsigned timestamp = ktime_to_ms(ktime_get_boottime());
	int i, dwork_timeout = 10000;

	if (param != PIN_CONFIG_INPUT_DEBOUNCE || arg < SX1503_MIN_DEBOUNCE)
		return -ENOTSUPP;

	chip->debounce[offset] = arg;
	chip->timestamp[offset] = timestamp;
	for (i = 0; i < 16; ++i) {
		if (chip->debounce[i] > 0 && chip->debounce[i] < dwork_timeout) {
			dwork_timeout = chip->debounce[i];
		}
	}
	chip->dwork_timeout = dwork_timeout;
	return 0;
}


static int sx1503_reset(struct sx1503_chip *chip)
{
	int err;
	u8 regSLA = 0xFF, regSHA = 0xFF, regSLB = 0xFF, regSHB = 0xFF;
	sx1503_i2c_read(chip->client, 0x0A, &regSHB);
	sx1503_i2c_read(chip->client, 0x0B, &regSHA);
	sx1503_i2c_read(chip->client, 0x0C, &regSLB);
	sx1503_i2c_read(chip->client, 0x0D, &regSLA);

	if (regSLA == 0xFF && regSLB == 0xFF && regSHA == 0xFF && regSHB == 0xFF)
		return -EINVAL;

	err = i2c_smbus_write_byte_data(chip->client,
					0x03, SX1503X_DIRA_DEFAULT);
	if (err < 0)
		return err;

	err = i2c_smbus_write_byte_data(chip->client,
					0x01, SX1503X_DATAA_DEFAULT);
	if (err < 0)
		return err;


	err = i2c_smbus_write_byte_data(chip->client,
					0x02, SX1503X_DIRB_DEFAULT);
	if (err < 0)
		return err;

	err = i2c_smbus_write_byte_data(chip->client,
					0x00, SX1503X_DATAB_DEFAULT);
	if (err < 0)
		return err;

    return err;
}


static void sx1503_gpio_work_func(struct work_struct *work)
{
	struct sx1503_chip *chip =
		container_of(work, struct sx1503_chip, dwork.work);
	int i, n, err, sub_irq;
	u32 dwork_timeout = SX1503_MIN_DEBOUNCE;
	const int subs_irq[] = { 
			0, 0, 1, 2, 2, 3, 4, 4, 5, 6, 6, 7, -1, -1, -1, 8};
	u8 val;
	u32 timestamp = ktime_to_ms(ktime_get_boottime());
	mutex_lock(&chip->lock);

	for (i = 0; i <= 1; ++i) {
		err = sx1503_i2c_read(chip->client,
				chip->dev_cfg->reg_irq_src - i,
				&val);
		if (err < 0 || val == 0xFF)
			continue;

		sx1503_i2c_write(chip->client,
				chip->dev_cfg->reg_irq_src - i,
				val);

		for (n = 0; n < 8; ++n) {
			if (val & (1 << n)) {
				sub_irq = subs_irq[(i * 8) + n];
				if (sub_irq >= 0 && timestamp >= (chip->timestamp[sub_irq] + chip->debounce[sub_irq])) {
					int value = (chip->dev_sense & (0x3 << (sub_irq * 2))) >> (sub_irq * 2);
					gpio_sx_create_event(&chip->gpio_chip, sx1503_gpio_names[sub_irq], value,
										chip->debounce[sub_irq], timestamp);
					chip->timestamp[sub_irq] = chip->old_time;
					handle_nested_irq(chip->irq_base + sub_irq);
				}
			}
		}
	}
	if (chip->dwork_timeout > SX1503_MIN_DEBOUNCE)
		dwork_timeout = chip->dwork_timeout;
	chip->old_time = timestamp;
	mutex_unlock(&chip->lock);
	schedule_delayed_work(&chip->dwork, msecs_to_jiffies(dwork_timeout/4));
}


static int sx1503_gpio_to_irq(struct gpio_chip *gc, unsigned offset)
{
	struct sx1503_chip *chip;

	chip = container_of(gc, struct sx1503_chip, gpio_chip);

	if (offset >= chip->dev_cfg->ngpios)
		return -EINVAL;

	if (chip->irq_base < 0)
		return -EINVAL;

	return chip->irq_base + offset;
}


static void sx1503_irq_mask(struct irq_data *d)
{
	struct sx1503_chip *chip = irq_data_get_irq_chip_data(d);
	unsigned n;

	n = d->irq - chip->irq_base;
	chip->irq_masked |= (1 << n);
	chip->irq_update = n;
}


static void sx1503_irq_unmask(struct irq_data *d)
{
	struct sx1503_chip *chip = irq_data_get_irq_chip_data(d);
	unsigned n;

	n = d->irq - chip->irq_base;
	chip->irq_masked &= ~(1 << n);
	chip->irq_update = n;
}


static int sx1503_irq_set_type(struct irq_data *d, unsigned int flow_type)
{
	struct sx1503_chip* chip = irq_data_get_irq_chip_data(d);
	unsigned n, val = 0;

	if (flow_type & (IRQ_TYPE_LEVEL_HIGH | IRQ_TYPE_LEVEL_LOW))
		return -EINVAL;

	n = d->irq - chip->irq_base;

	// TODO: default value: RISING 0x01, FALLING: 0x02
	if (flow_type & IRQ_TYPE_EDGE_RISING)
		val |= 0x2;
	if (flow_type & IRQ_TYPE_EDGE_FALLING)
		val |= 0x1;

	chip->irq_sense &= ~(3UL << (n * 2));
	chip->irq_sense |= val << (n * 2);
	chip->irq_update = n;

	return 0;
}


static void sx1503_irq_bus_lock(struct irq_data *d)
{
	struct sx1503_chip *chip = irq_data_get_irq_chip_data(d);
	mutex_lock(&chip->lock);
}


static void sx1503_irq_bus_sync_unlock(struct irq_data *d)
{
	struct sx1503_chip *chip = irq_data_get_irq_chip_data(d);
	unsigned n;

	if (chip->irq_update == NO_UPDATE_PENDING)
		goto out;

	n = chip->irq_update;
	chip->irq_update = NO_UPDATE_PENDING;

	if (chip->dev_sense == chip->irq_sense &&
	    chip->dev_sense == chip->irq_masked)
		goto out;

	chip->dev_sense = chip->irq_sense;
	chip->dev_masked = chip->irq_masked;

	if (chip->irq_masked & (1 << n)) {
		sx1503_write_cfg(chip, sx1503_gpio[n].gpio, 1, chip->dev_cfg->reg_irq_mask, 1);
		sx1503_write_cfg(chip, sx1503_gpio[n].gpio, 2, chip->dev_cfg->reg_sense, 0);
	} else {
		sx1503_write_cfg(chip, sx1503_gpio[n].gpio, 1, chip->dev_cfg->reg_irq_mask, 0);
		sx1503_write_cfg(chip, sx1503_gpio[n].gpio, 2, chip->dev_cfg->reg_sense,
				 chip->irq_sense >> (n * 2));
	}

out:
	mutex_unlock(&chip->lock);

}


static void sx1503_init_chip(struct sx1503_chip *chip,
			struct i2c_client *client,
			int irq)
{
	mutex_init(&chip->lock);

	chip->client                     = client;
	chip->dev_cfg                    = &sx1503_devices;
	chip->gpio_chip.label            = client->name;
	chip->gpio_chip.direction_input  = sx1503_gpio_direction_input;
	chip->gpio_chip.direction_output = sx1503_gpio_direction_output;
	chip->gpio_chip.get              = sx1503_gpio_get;
	chip->gpio_chip.set              = sx1503_gpio_set;
	chip->gpio_chip.set_config       = sx1503_gpio_set_config;
	chip->gpio_chip.base             = -1;
	chip->gpio_chip.can_sleep        = true;
	chip->gpio_chip.names            = sx1503_gpio_names;
	chip->gpio_chip.ngpio            = chip->dev_cfg->ngpios;
	chip->gpio_chip.to_irq           = sx1503_gpio_to_irq;


	chip->irq_chip.name                = "GPIO";
	chip->irq_chip.irq_mask            = sx1503_irq_mask;
	chip->irq_chip.irq_unmask          = sx1503_irq_unmask;
	chip->irq_chip.irq_set_type        = sx1503_irq_set_type;
	chip->irq_chip.irq_bus_lock        = sx1503_irq_bus_lock;
	chip->irq_chip.irq_bus_sync_unlock = sx1503_irq_bus_sync_unlock;
	chip->irq_base                     = irq;

	chip->irq_masked                  = ~0;
	chip->irq_sense                   = 0;
	chip->dev_masked                  = ~0;
	chip->dev_sense                   = 0;
	chip->irq_update                  = NO_UPDATE_PENDING;
	chip->dwork_timeout               = SX1503_DEFAULT_DEBOUNCE;
}


static int sx1503_install_irq_chip(struct sx1503_chip* chip,
			int irq_base)
{
	unsigned n;
	int irq, ret;

	for (n = 0; n < chip->dev_cfg->ngpios; ++n) {
		irq = irq_base + n;
		irq_set_chip_data(irq, chip);
		irq_set_chip_and_handler(irq, &chip->irq_chip, handle_edge_irq);
		irq_set_nested_thread(irq, 1);
#ifdef CONFIG_ARM
		set_irq_flags(irq, IRQF_VALID);
#else
		irq_set_noprobe(irq);
#endif
	}
	return ret;
}


static void sx1503_remove_irq_chip(struct sx1503_chip *chip)
{
	unsigned n;
	unsigned irq;

	for (n = 0; n < chip->dev_cfg->ngpios; ++n) {
		irq = chip->irq_base + n;
		irq_set_chip_and_handler(irq, NULL, NULL);
	}
}


static int sx1503_gpio_export(struct sx1503_chip *chip)
{
	int ret = 0;
	int i = 0;

	for (i = 0; i < chip->gpio_chip.ngpio; ++i) {
		ret = gpio_request(chip->gpio_chip.base + i, sx1503_gpio_names[i]);
		if (ret) {
			printk(KERN_ERR "sx1503_custom: ERROR: request GPIOs\n");
			goto err;
		}
		ret = gpio_export(chip->gpio_chip.base + i, true);
		if (ret) {
			printk(KERN_ERR "sx1503_custom: ERROR: export GPIOs\n");
			goto err;
		}

		if (!sx1503_gpio[i].input) {
			ret = gpio_direction_output(chip->gpio_chip.base + i, 0);
			if (ret) {
				printk(KERN_ERR "sx1503_custom: ERROR: set output GPIOs\n");
				goto err;
			}
		}
		gpiod_set_debounce(gpio_to_desc(chip->gpio_chip.base + i), SX1503_DEFAULT_DEBOUNCE);
	}
err:
	return ret;
}


static void sx1503_gpio_unexport(struct sx1503_chip* chip)
{
	int i = 0;
	for (i = 0; i < chip->gpio_chip.ngpio; ++i) {
		gpio_unexport(chip->gpio_chip.base + i);
		gpio_free(chip->gpio_chip.base + i);
	}
}


static int sx1503_probe(struct i2c_client *client,
				const struct i2c_device_id *id)
{
	static const u32 i2c_funcs = I2C_FUNC_SMBUS_BYTE_DATA |
				     I2C_FUNC_SMBUS_WRITE_WORD_DATA;
	struct sx1503_chip *chip;
	struct device_node *np = client->dev.of_node;

	int rc;
	int irq;
	int irq_gpio;

	if (!i2c_check_functionality(client->adapter, i2c_funcs))
		return -ENOSYS;

	chip = devm_kzalloc(&client->dev,
		sizeof(struct sx1503_chip), GFP_KERNEL);
	if (!chip)
		return -ENOMEM;

	irq_gpio = of_get_named_gpio(np, "int", 0);
	if (irq_gpio > 0) {
		rc = gpio_is_valid(irq_gpio);
		if (!rc) {
			printk(KERN_ERR "sx1503_gpio not valid %d\n", irq_gpio);
			goto err;
		}

		rc = gpio_request(irq_gpio, "sx1503_int");
		if (rc) {
			printk(KERN_ERR "sx1503_gpio gpio %d request failed\n", irq_gpio);
			goto err;
		}

		gpio_direction_input(irq_gpio);

		irq = gpio_to_irq(irq_gpio);

		if (irq < 0) {
			rc = irq;
			printk(KERN_ERR "sx1503_gpio Can't get irq for %d gpio\n", irq_gpio);
			goto err;
		}
		chip->irq_gpio = irq_gpio;
	} else {
		rc = -EINVAL;
		goto err;
	}

	sx1503_init_chip(chip, client, irq);
	rc = sx1503_reset(chip);
	if (rc < 0)
		goto err;

	rc = gpiochip_add(&chip->gpio_chip);
	if (rc)
		goto err;;

	rc = sx1503_gpio_export(chip);
	if (rc)
		goto probe_fail_post_gpiochip_add;

	if (irq >= 0) {
		rc = sx1503_install_irq_chip(chip, irq);
		if (rc < 0)
			goto probe_fail_post_gpiochip_add;
	}


	i2c_set_clientdata(client, chip);

	INIT_DELAYED_WORK(&chip->dwork, sx1503_gpio_work_func);
	schedule_delayed_work(&chip->dwork, msecs_to_jiffies(SX1503_DEFAULT_DEBOUNCE));

	return 0;

probe_fail_post_gpiochip_add:
	gpiochip_remove(&chip->gpio_chip);
err:
	if (irq_gpio > 0) {
		gpio_free(irq_gpio);
	}
	return rc;
}


static int sx1503_remove(struct i2c_client *client)
{
	struct sx1503_chip *chip = i2c_get_clientdata(client);

	cancel_delayed_work_sync(&chip->dwork);

	sx1503_gpio_unexport(chip);

	if (chip->irq_base >= 0)
		sx1503_remove_irq_chip(chip);

	if (chip->irq_gpio > 0) {
		gpio_free(chip->irq_gpio);
	}

	gpiochip_remove(&chip->gpio_chip);

	return 0;
}


static struct i2c_driver sx1503_driver = {
	.driver = {
		.name = "sx1503",
		.owner = THIS_MODULE
	},
	.probe    = sx1503_probe,
	.remove   = sx1503_remove,
	.id_table = sx1503_id,
};


static int __init sx1503_init(void)
{
	return i2c_add_driver(&sx1503_driver);
}
subsys_initcall(sx1503_init);


static void __exit sx1503_exit(void)
{
	return i2c_del_driver(&sx1503_driver);
}
module_exit(sx1503_exit);


MODULE_AUTHOR("Dmitry Peresypkin <dperesypkin@radiofid.ru>");
MODULE_DESCRIPTION("Custom driver for SX1503 I2C GPIO Expanders");
MODULE_LICENSE("GPL v2");
