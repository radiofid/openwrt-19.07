#!/bin/sh

m_count(){
    case $1 in
        1) echo 1;;
        2) echo 2;;
        3) echo 3;;
        4) echo 4;;
    esac
}
m_cat(){
    case $1 in
        L|l) echo "Cat4";;
        A|a) echo "Cat6";;
        C|c) echo "CDMA";;
    esac
}

m_category_sub(){
    local m="$1"
    local _M
    if [ ${#m} -ge 4 ]; then
        for __mm in $(seq 1 $(m_count ${m:1:1})); do
            _M="${_M}${m:0:1}"
        done
        for __mm in $(seq 1 $(m_count ${m:3:1})); do
            _M="${_M}${m:2:1}"
        done
    elif [ ${#m} -ge 2 ]; then
        for __mm in $(seq 1 $(m_count ${m:1:1})); do
            _M="${_M}${m:0:1}"
        done
    fi
    echo $_M
}

parse_modules(){
    local m="$1"
    m=${m//00/}
    sub_cats=$(m_category_sub $m)
    BOARD_MODULES=${#sub_cats}
    case $BOARD_MODULES in
        4) SIM_SWITCH=n;;
        *) SIM_SWITCH=y;;
    esac

    echo "BOARD_MODULES=$BOARD_MODULES"
    for _m in $(seq 1 $BOARD_MODULES); do
        echo "MODULE${_m}=$(m_cat ${sub_cats:$((_m - 1)):1})"
    done
    echo "SIM_SWITCH=$SIM_SWITCH"
}

parse_features(){
    local p="$1"
    local pos c char
    BOARD_WIFI_MODULES=0
    for pos in $(seq 1 ${#p}); do
        c=$(($pos -1))
        char=${p:$c:1}
        case $char in
            A)
                BOARD_WIFI_MODULES=$(($BOARD_WIFI_MODULES + 1))
                echo 'WIFI'$BOARD_WIFI_MODULES'="5"'
            ;;
            W)
                BOARD_WIFI_MODULES=$(($BOARD_WIFI_MODULES + 1))
                echo 'WIFI'$BOARD_WIFI_MODULES'="2.4"'
            ;;
            n)
                echo "BOARD_GPS=y"
            ;;
        esac
    done
    [ $BOARD_WIFI_MODULES -ge 1 ] && echo "BOARD_WIFI_MODULES=$BOARD_WIFI_MODULES"

    # R50 only
    echo "BOARD_RS485=1"
    echo "BOARD_ETHERS=5"
    echo "BOARD_GPI=3"
}

cursed_models(){
	model="$1"
	spec="${model##*-}"

	echo "SERIES=${model%%-*}"
	parse_modules "${spec%%.*}"
	parse_features "${spec##*.}"
}
