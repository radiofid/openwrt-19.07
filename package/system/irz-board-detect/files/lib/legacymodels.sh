#!/bin/sh

. /lib/uboot-envtools.sh

legacy_models() {
	model="$1"
	modcount="$2"
	is_wwan_onboard="$3"

	poe="$(ubootenv_get_var board_poe)"
	sfp="$(ubootenv_get_var board_sfp)"

	echo "BOARD_MODULES=${modcount}"  # +
	echo "MODULE_ONBOARD=${is_wwan_onboard}" # +
	if [ "$modcount" -gt "1" ]; then
		echo "SIM_SWITCH=n"
	else
		echo "SIM_SWITCH=y"
	fi
	case ${model:1:1} in
		L) echo "MODULE1=Cat4";;
		A) echo "MODULE1=Cat6";;
		C) echo "MODULE1=CDMA";;
		U) echo "MODULE1=UMTS";;
	esac
	if [ $modcount -eq 2 ]; then
		case ${model:4:1} in
			l) echo "MODULE2=Cat4";;
			a) echo "MODULE2=Cat6";;
			c) echo "MODULE2=CDMA";;
			u) echo "MODULE2=UMTS";;
		esac
	fi

	case $model in
		*w|*w*)
			echo "BOARD_WIFI_MODULES=1"
			echo 'WIFI1="2.4"'
		;;
	esac

	case "${model:2:2}" in
		01)
			echo "BOARD_POE=pin"
			echo "BOARD_POE_IN=1"
			echo "BOARD_ETHERS=1"
		;;
		20)
			echo "BOARD_POE=pin"
			echo "BOARD_POE_IN=1"
			echo "BOARD_ETHERS=4"
		;;
		21|24)
			echo "BOARD_GPIO=7"
			echo "BOARD_RS232=1"
			echo "BOARD_RS485=1"
			echo "BOARD_ETHERS=4"
			echo "BOARD_POE=pin"
			echo "BOARD_POE_IN=1"
		;;
		22)
			echo "BOARD_GPIO=7"
			echo "BOARD_GPS=y"
			echo "BOARD_RS232=1"
			echo "BOARD_RS485=1"
			echo "BOARD_ETHERS=4"
			echo "BOARD_POE=pin"
			echo "BOARD_POE_IN=1"
		;;
		23)
			echo "BOARD_RS232=1"
			echo "EXTRA_COMBO_SERIAL=1"
			echo "BOARD_ETHERS=2"
			echo "BOARD_POE=pin"
			echo "BOARD_POE_IN=1"
		;;
		25)
			echo "BOARD_GPIO=7"
			echo "BOARD_GPI=4"
			echo "BOARD_GPO=4"
			echo "BOARD_RS232=1"
			echo "BOARD_RS485=1"
			echo "EXTRA_COMBO_SERIAL=3"
			echo "BOARD_ETHERS=4"
			echo "BOARD_POE=pin"
			echo "BOARD_POE_IN=1"
		;;
		26)
			echo "BOARD_GPIO=7"
			echo "BOARD_GPI=4"
			echo "BOARD_GPO=4"
			echo "BOARD_GPS=y"
			echo "BOARD_RS232=1"
			echo "BOARD_RS485=1"
			echo "EXTRA_COMBO_SERIAL=3"
			echo "BOARD_ETHERS=4"
			echo "BOARD_POE=pin"
			echo "BOARD_POE_IN=1"
		;;
		27)
			echo "BOARD_GPIO=7"
			echo "BOARD_RS232=1"
			echo "BOARD_RS485=1"
			echo "BOARD_ETHERS=1"
			echo "BOARD_POE=pin"
			echo "BOARD_POE_IN=1"
		;;
		4[0-9])
			echo "BOARD_GPIO=3"
			echo "BOARD_RS232=1"
			echo "BOARD_RS485=1"
			echo "BOARD_ETHERS=5"
			echo "BOARD_USB=1"
			if [ "$poe" == "y" ]; then
				echo "BOARD_POE=pout"
				echo "BOARD_POE_OUT=1"
			fi

			if [ "$sfp" == "y" ]; then
				echo "BOARD_SFP=1"
			fi
		;;
	esac
}
