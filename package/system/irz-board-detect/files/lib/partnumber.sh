#!/bin/sh

# Defaults
conf_dir="/lib/naming"
conf_file="devices.json"

# Statics
NL=$'\n'
CHARS="0ABCDEFGHIJKLMNOPQRSTUVWXYZ"

# Globals
config=""
config_select=""

capabilities=""
raw_capabilities=""

pn_chars=""
cur_char=""

# PN string parsing
next_char() {
	cur_char="${pn_chars%"${pn_chars#?}"}"
	pn_chars="${pn_chars#?}"
	[ -z "$cur_char" ] && cur_char="0"
}

char2int() {
	local tmp="${CHARS%"$1"*}"	# Remove characters from CHARS starting from argument
	echo "${#tmp}"				# New string length will be the desired integer value
}

pn_decompress() {
	echo "$1" |
		# Isolate all numbers as lines formatted as ^.NNN$
		# Any non number parts are printed verbatim
		sed 's/[1-9][0-9]*/\n.&\n/g' |
			while read -r part
			do
				case "$part" in
					.*)							# Line starts with . is a number
						# %.NNNd formats number with NNN leading zeroes
						printf "%${part}d" "0"
						;;
					*)							# All other lines are not numbers
						printf "%s" "$part"		# Line is not number, just print it
						;;
				esac
			done
}

# Json variable loading
json_get() {
	local json_data="$1"
	local json_path="$2"
	local value

	value="$(echo "$json_data" | jsonfilter -e "res=@.$json_path")"
	eval "echo ${value#*res=}"
}

# Raw capabilities processing
raw_capability_set_one() {
	raw_capabilities="${raw_capabilities}${NL}$*"
}

raw_capability_set_json_value() {
	local json_data="$1"
	local capability_name="$2"
	local json_path="$3"
	local capability_type="$4"

	local capability_value

	next_char
	[ "$cur_char" = "0" ] && cur_char="none"
	capability_value=$(json_get "$json_data" "${json_path}.values.${cur_char}")

	raw_capability_set_one "$capability_type" "$capability_name" "$capability_value"
}

raw_capability_set_json_several() {
	local json_data="$1"
	local capability_name="$2"
	local json_path="$3"

	local nargs
	local capability_arg

	next_char
	nargs="$(char2int "$cur_char")"
	raw_capability_set_one "additive" "$capability_name" "$nargs"

	json_path="${json_path}.several"
	capability_arg="$(json_get "$json_data" "${json_path}.capability")"

	for _ in $(seq "$nargs"); do
		raw_capability_set_json \
			"$json_data" "$capability_arg" "$json_path" "iterative"
	done
}

raw_capability_set_json_repeat() {
	local json_data="$1"
	local capability_name="$2"
	local json_path="$3"

	local nrep
	local capability_arg
	local old_raw
	local arg_raw

	next_char
	nrep="$(char2int "$cur_char")"
	raw_capability_set_one "additive" "$capability_name" "$nrep"

	json_path="${json_path}.repeat"
	capability_arg="$(json_get "$json_data" "${json_path}.capability")"

	old_raw="$raw_capabilities"
	raw_capabilities=""
	raw_capability_set_json \
		"$json_data" "$capability_arg" "$json_path" "iterative"
	arg_raw="$raw_capabilities"
	raw_capabilities="$old_raw"

	for _ in $(seq "$nrep"); do
		raw_capabilities="${raw_capabilities}${NL}${arg_raw}"
	done
}

raw_capability_set_json_number() {
	local capability_name="$2"
	local capability_type="$4"

	local capability_value

	next_char
	capability_value="$(char2int "$cur_char")"

	raw_capability_set_one "$capability_type" "$capability_name" "$capability_value"
}

raw_capability_set_json() {
	local json_data="$1"
	local capability_name="$2"
	local json_path="$3"
	local capability_type="$4"

	if [ -z "$capability_name" ]; then
		echo "Empty capability name" >&2
		exit 2
	fi
	[ -z "$json_path" ] &&
		json_path="values[@['capability']='$capability_name']"
	[ -z "$capability_type" ] &&
		capability_type="normal"

	local value_type
	value_type="$(json_get "$json_data" "$json_path")"
	case "$value_type" in
		*"values"*)
			raw_capability_set_json_value \
				"$json_data" "$capability_name" "$json_path" "$capability_type"
			;;
		*"several"*)
			raw_capability_set_json_several \
				"$json_data" "$capability_name" "$json_path" "$capability_type"
			;;
		*"repeat"*)
			raw_capability_set_json_repeat \
				"$json_data" "$capability_name" "$json_path" "$capability_type"
			;;
		*)
			raw_capability_set_json_number \
				"$json_data" "$capability_name" "$json_path" "$capability_type"
			;;
	esac
}

# Capabilities processing
capabilities_set_one() {
	local capability_type="$1"
	local name="$2"
	local value="$3"

	[ -z "$name" ] && return

	if [ "$capability_type" = "additive" ]; then
		local last_value
		last_value="$(echo "$capabilities" | sed "
				/^${name}=[0-9]*/h;\$!d;g;		# Hold matches until last line
				s/^${name}=\\([0-9]*\\)/\\1/;	# On last line get the value
			")"

		value="$((last_value + value))"

		if [ -n "$last_value" ]; then
			capabilities="$(echo "$capabilities" |
				sed "s/${name}=.*/${name}=${value}/")"
			return
		fi
	fi

	if [ "$capability_type" = "iterative" ]; then
		local prefix="${name%"%i"*}"
		local suffix="${name#*"%i"}"
		local last_index
		last_index="$(echo "$capabilities" | sed "
				/^${prefix}[0-9]*${suffix}=.*/h;\$!d;g;		# DITTO additive
				s/^${prefix}\\([0-9]*\\)${suffix}=.*/\\1/;	# DITTO additive
			")"

		name="${prefix}$((last_index + 1))${suffix}"
	fi

	capabilities="${capabilities:+${capabilities}${NL}}${name}=${value}"
}

capabilities_set_many() {
	local many_raw="$1"
	local args

	while read -r args; do
		capabilities_set_one $args
	done <<EOF
$many_raw
EOF
}

capabilities_set_sequence() {
	local sequence="$1"
	local json_sequence="$2"
	local json_values="$3"
	local capability_sequence
	local capability_name

	pn_chars=$(pn_decompress "$sequence")
	capability_sequence=$(json_get "$json_sequence" "capabilities[*]")
	raw_capabilities=""

	for capability_name in $capability_sequence; do
		raw_capability_set_json "$json_values" "$capability_name"
	done

	capabilities_set_many "$raw_capabilities"
}

# Partnumber splitting
pn_family() {
	local partnumber="$1"
	local family="${partnumber%"${partnumber#[A-Z]}"}"

	if [ -z "$family" ]; then
		echo "Bad partnumber (device family)" >&2
		exit 1
	fi

	echo "$family"
}

pn_series() {
	local partnumber="$1"
	local series="${partnumber%%-*}"

	echo "$series"
}

pn_base() {
	local partnumber="$1"
	local base

	base="${partnumber#"${partnumber%%-*}"}"
	base="${base#-}"
	base="${base%%.*}"

	if [ -n "${base//[0-9A-Z]/}" ]; then
		echo "Bad partnumber (base sequence)" >&2
		exit 1
	fi

	echo "$base"
}

pn_spec() {
	local partnumber="$1"
	local spec

	spec="${partnumber#"${partnumber%%-*}"}"
	spec="${spec#-}"
	spec="${spec#"${spec%%.*}"}"
	spec="${spec#.}"

	if [ -n "${spec//[0-9A-Z]/}" ]; then
		echo "Bad partnumber (spec sequence)" >&2
		exit 1
	fi

	echo "$spec"
}

# Configuration loading
load_conf() {
	local file_name
	file_name="$conf_dir/$1"
	if [ ! -f "$file_name" ]; then
		echo "Configuration file $file_name does not exist" >&2
		exit 1
	fi
	cat "$file_name"
}

load_description() {
	local json_path="$1"
	local file_name

	file_name="$(json_get "$config" "${config_select}.${json_path}")"

	if [ -z "$file_name" ]; then
		echo "No ${json_path} config found in ${config_select}" >&2
		exit 1
	fi

	load_conf "$file_name"
}

# Main function
partnumber_capabilities() {
	local partnumber="$1"
	[ -n "$2" ] && conf_dir="$2"
	[ -n "$3" ] && conf_file="$3"

	local family
	local series
	local base
	local spec

	family="$(pn_family "$partnumber")" || exit $?
	series="$(pn_series "$partnumber")" || exit $?
	base="$(pn_base "$partnumber")" || exit $?
	spec="$(pn_spec "$partnumber")" || exit $?

	config="$(load_conf "$conf_file")" || exit $?
	config_select="devices.${family}"

	local values
	local base_sequence
	local spec_sequence

	values="$(load_description "values")" || exit $?
	base_sequence="$(load_description "base")" || exit $?
	spec_sequence="$(load_description "opt.${series}")" || exit $?

	capabilities=""
	capabilities_set_one "normal" "SERIES" "${series}"
	capabilities_set_sequence "$base" "$base_sequence" "$values"
	capabilities_set_sequence "$spec" "$spec_sequence" "$values"

	echo "$capabilities"
}

# Exec
partnumber_capabilities "$@"
