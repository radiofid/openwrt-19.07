#!/bin/sh

# Static
CHARS="0ABCDEFGHIJKLMNOPQRSTUVWXYZ"
conf_dir="/lib/naming"
NL=$'\n'

# Globals
values_file=""

short_values_format=false
interactive_mode=false
description_mode=false
device_number=""

capability_values=""
shifts=0

pn_part=""
result_part=""

print_dash () {
    echo "--------------------------" >&2
}

# Help
help_exit() {
    local exitcode="$1"
    local devices_list="$2"

    echo "Usage: $0 [OPTIONS] <-i|DEVICE <CAPABILITY <<VALUE>...> > ...>"
    echo " "
    echo "Options:"
    echo "  -v, --rev <REV>           REV: Append revision suffix to DEVICE."
    echo "                              There is no difference between using this option"
    echo "                              to set the revision and including it in the DEVICE."
    echo "  -i, --interact            Interactive mode."
    echo "  -s, --short               Short values format."
    echo "                              In short values format, values for capabilities are"
    echo "                              entered by their keys instead of their full names."
    echo "  -h, --help [DEVICE]       Show this help message"
    echo "                              With DEVICE print available capabilities for DEVICE."
    echo "                              UNABLE IN INTERACTIVE MODE."
    echo " "
    echo "Positional arguments (ignored in interactive mode):"
    echo "  DEVICE                    The first positinal argument sets the device, to be"
    echo "                              used when interactive mode is not set."
    echo "  CAPABILITY <<VALUE>...>   Other positinal arguments set capabilities and their"
    echo "                              values. If the capability has a dependant capability,"
    echo "                              its value(s) must also be provided as additional"
    echo "                              arguments."
    echo " "
    [ -n "$devices_list" ] &&  echo "DEVICE list: $devices_list"
    echo " "
    exit "$exitcode"
}

# Json variables loading
json_get() {
    local json_data="$1"
    local json_condition="$2"

    echo "$json_data" | jsonfilter -e "$json_condition"
}

json_get_keys() {
    local json_data="$1"
    local json_condition="$2"
    local res

    eval "$(json_get "$json_data" "res=$json_condition")"
    echo "$res"
}

json_check_empty() {
    local json_data="$1"
    local json_path="$2"
    local result="$(json_get "$json_data" "$json_path")"

    if [ -z "$result" ]; then
        echo "Error: JSON path [$json_path] returned empty value" >&2
        exit 1
    fi

    echo "$result"
}

# Process command line arguments
process_options() {
    local devices_list="$1"
    shift

    [ "$#" -eq 0 ] && help_exit 1 "$devices_list" >&2

    local opts=""
    local opt=""
    local revision=""

    while [ "$#" -gt 0 ]; do
        case $1 in
            -i|--interact)
                opts="i"
                ;;
            -s|--short)
                opts="s"
                ;;
            -v|--rev)
                opts="v"
                ;;
            -h|--help)
                opts="h"
                ;;
            --*)
                echo "Invalid option: $1" >&2
                help_exit 1 "$devices_list" >&2
                ;;
            -*)
                opts="${1#?}"
                ;;
                
            *)
                break
                ;;
        esac
        shift
        shifts=$((shifts + 1))

        while [ -n "$opts" ]; do
            opt="${opts%"${opts#?}"}"
            opts="${opts#?}"
            case "$opt" in
                i)
                    interactive_mode=true
                    ;;
                s)
                    short_values_format=true
                    ;;
                v)
                    if [ -n "$opts" ]; then
                        revision="$opts"
                        opts=""
                    else
                        revision="$1"
                        shift
                        shifts=$((shifts + 1))
                    fi
                    ;;
                h)
                    description_mode=true
                    ;;
                *)
                    echo "Invalid option: -$opt" >&2
                    help_exit 1 "$devices_list" >&2
                    ;;
            esac
        done
    done

    if [ "$#" -gt 0 ]; then
        device_number="$1"
        shift
        shifts=$((shifts + 1))
    fi

    if [ "$description_mode" = true ]; then
        if [ "$interactive_mode" = true ]; then
            echo "Unable to print available capabilities with interactive mode!" >&2
            exit 1
        elif [ -z "$device_number" ]; then
            help_exit 0 "$devices_list" >&2
        fi
    fi

    if [ -z "$device_number" ] && [ "$interactive_mode" = false ]; then
        echo "Missing argument: DEVICE" >&2
        help_exit 1 "$devices_list" >&2
    fi

    [ -n "$revision" ] && device_number="${device_number}${revision}"

    if [ "$interactive_mode" = false ] && ! echo "$devices_list" | grep -qw "$device_number"; then
        echo "Invalid value [$device_number]. Enter possible values from" >&2
        echo "DEVICE list: $devices_list" >&2
        exit 1
    fi
}

get_arg_capabilities () {
    [ "$interactive_mode" = true ] && return
    [ "$description_mode" = true ] && return

    local arguments_list=""
    add_arg () {
        local new_value="$1"
        arguments_list="${arguments_list} ${new_value}"

        local count="$(echo "$arguments_list" | wc -w)"
        [ "$count" -gt 4 ] && arguments_list="... ${arguments_list#* * }"
    }

    print_context () {
        local arguments_list="$1"
        shift
        local error_arg="${arguments_list##* }"
        arguments_list="${arguments_list% *}"

        local color_fatal=$'\033[0;31m'
        local color_off=$'\033[0m'
        local suffix="${1:+" $1"}${2:+" $2"}${3:+" $3"}${4:+ ...}"

        echo "Context: [ $arguments_list ${color_fatal}${error_arg}${color_off}${suffix} ]" >&2
    }

    while [ "$#" -gt 0 ]; do

        local value_types=""
        capabilities_path="@.values[@['capability']='$1']"
        value_types="$(json_get_keys "$values_file" "$capabilities_path")"

        local capability="$1"
        add_arg "$capability"
        shift
        [ -z "$value_types" ] && {
            echo "Can't find capability [$capability]" >&2
            print_context "$arguments_list" "$@"
            exit 1
        }

        local arg="$1"
        add_arg "$arg"
        shift
        [ -z "$arg" ] && {
            echo "Missing argument for [$capability]" >&2
            exit 1
        }

        capability_values="${capability_values}${NL}${capability} ${arg}"

        local dependent_capability=""
        case "$value_types" in
            *"several"*)
                dependent_capability="$(json_get \
                    "$values_file" "$capabilities_path.several.capability")"

                if [ -n "${arg##*[!0-9]*}" ]; then
                    for device_num in $(seq "$arg"); do
                        [ -z "$1" ] && {
                            echo "Missing argument for [${dependent_capability/"%i"/"$device_num"}]" >&2
                            exit 1
                        }
                        capability_values="${capability_values}${NL}${dependent_capability/"%i"/"$device_num"} $1"
                        add_arg "$1"
                        shift
                    done
                else
                    echo "Argument [$arg] is not a number" >&2
                    print_context "$arguments_list" "$@"
                    exit 1
                fi
                ;;

            *"repeat"*)
                dependent_capability="$(json_get \
                    "$values_file" "$capabilities_path.repeat.capability")"
                if [ -n "${arg##*[!0-9]*}" ]; then
                    [ -z "$1" ] && {
                        echo "Missing argument for [${dependent_capability/"%i"/""}]" >&2
                        exit 1
                    }
                    capability_values="${capability_values}${NL}${dependent_capability/"%i"/""} $1"
                    add_arg "$1"
                    shift
                else
                    echo "Argument [$arg] is not a number" >&2
                    print_context "$arguments_list" "$@"
                    exit 1
                fi
                ;;
        esac
    done
}

# Get configs for DEVICE
safe_cat() {
    local file_path="$1"
    if [ ! -f "$file_path" ]; then
        echo "Error: Configuration file [$file_path] not found" >&2
        exit 1
    fi

    cat "$file_path"
}

get_device_config () {
    local conf_file="$1"
    local file_type="$2"
    local device_type="${device_number:0:1}"

    local json_path
    local file_path

    case "$file_type" in
        "base")
            json_path="base"
            ;;

        "device")
            json_path="opt.$device_number"
            ;;

        "values")
            json_path="values"
            ;;
    esac

    file_path="$conf_dir/$(json_check_empty \
        "$conf_file" "@.devices.$device_type.$json_path")" || exit $?

    safe_cat "$file_path"
}

# Print
print_values() {
    local values="$1"

    if [ -z "$values" ]; then
        echo "Int [0,26]"
    elif [ "$short_values_format" = true ]; then
        json_get_keys "$values" "@"
    else
        json_get_keys "$values" "@.*"
    fi
}

print_capability_description() {
    local capability="$1"
    local description="$2"
    local values="$3"
    local dep_capability="$4"
    local dep_cap_type="$5"

    echo "Capability: $capability"
    echo "Description: $description"
    echo -n "Possible values: "
    print_values "$values"
    [ -n "$dep_capability" ] && echo "Dependent Capability: $dep_capability [$dep_cap_type]"
}

# Description mode
print_capability () {
    local capability="$1"
    local capabilities_path="$2"
    local value_types=""

    [ -z "$capabilities_path" ] && capabilities_path="@.values[@['capability']='$capability']"
    value_types="$(json_get_keys "$values_file" "$capabilities_path")"

    local description=$(json_get "$values_file" "$capabilities_path.description")
    local values="$(json_get "$values_file" "$capabilities_path.values")"
    local dependent_capability=""
    local dep_cap_type=""

    case "$value_types" in
        *"several"*)
            dep_cap_type="several"
            ;;

        *"repeat"*)
            dep_cap_type="repeat"
            ;;
    esac

    [ -n "$dep_cap_type" ] &&
        dependent_capability=$(json_get "$values_file" "$capabilities_path.$dep_cap_type.capability")

    echo "" >&2
    print_dash
    print_capability_description \
        "$capability" "$description" "$values" "$dependent_capability" "$dep_cap_type" >&2
    print_dash

    [ -n "$dependent_capability" ] &&
        print_capability "$dependent_capability" "$capabilities_path.$dep_cap_type"
}

print_capabilities_list() {
    local base_capabilities="$1"
    local device_capabilities="$2"
    local capabilities="${base_capabilities} ${device_capabilities}"

    for capability in $capabilities; do
        print_capability "$capability" "" 2>&1
    done
}

# Input DEVICE for interact_mode
input_device () {
    local devices_list="$1"
    local input_dev=""

    echo -e "\nDEVICE list: $devices_list" >&2
    print_dash
    while [ -z "$input_dev" ]; do
        echo -n "Enter value for DEVICE: " >&2
        read -r input_dev

        if ! echo "$devices_list" | grep -qw "$input_dev"; then
            echo "Invalid value [$input_dev]. Enter possible values from DEVICE list" >&2
            input_dev=""
        fi
    done
    print_dash

    device_number="$input_dev" 
}

# Processing values from a command line or manual input
validate_value() {
    local capability="$1"
    local config_values="$2"
    local value="$3"
    local valid_values=""

    [ -n "$config_values" ] && {
        if [ "$short_values_format" = true ]; then
            valid_values="$(json_get_keys "$config_values" "@")"
        else
            valid_values="$(json_get_keys "$config_values" "@.*")"
        fi
    }

    [ -z "$valid_values" ] && {
        is_integer=true
        valid_values="$(seq 0 26)"
    }

    if ! echo "$valid_values" | grep -qw "$value"; then
        if [ "$is_integer" = true ]; then
            echo "Invalid value [$value] for $capability. Possible values are: Integer in range [0, 26]" >&2
        else
            echo "Invalid value [$value] for $capability. Possible values are: $valid_values" >&2
        fi
        return 1
    fi
    return 0
}

input_to_char () {
    local capability="$1"
    local config_values="$2"
    local value="$3"

    if [ -z "$config_values" ]; then
        echo "$CHARS" | cut -c "$((value + 1))"
    else
        [ "$short_values_format" = false ] && {
            local keys=""
            keys="$(json_get_keys "$config_values" "@")"
            value=$(for key in $keys; do
                conf_value=$(json_get "$config_values" "@.$key")
                [ "$conf_value" = "$value" ] && echo "$key" && break
            done)
        }

        if [ "$value" = "none" ]; then
            echo "0"
        else
            echo "$value"
        fi
    fi
}

manual_input () {
    local capability="$1"
    local config_values="$2"
    local input_value=""

    while [ -z "$input_value" ]; do
        echo -n "Enter value for $capability: " >&2
        read -r input_value
        validate_value "$capability" "$config_values" "$input_value" ||
            input_value=""
    done

    echo "$input_value"
}

# Interactive and Command line modes
get_capability_value () {
    local is_interactive="$1"
    local capability="$2"
    local capabilities_path="$3"
    local value_types=""
    local conf_values
    local char_input
    local input

    [ -z "$capabilities_path" ] && capabilities_path="@.values[@['capability']='$capability']"
    value_types="$(json_get_keys "$values_file" "$capabilities_path")"
    conf_values=$(json_get "$values_file" "$capabilities_path.values")

    if [ "$is_interactive" = true ]; then
        echo "" >&2
        print_dash
        description=$(json_get "$values_file" "$capabilities_path.description")
        print_capability_description "$capability" "$description" "$conf_values" >&2
        
        input=$(manual_input "$capability" "$conf_values")
        print_dash
    else
        input=$(echo "$capability_values" | sed "/^$capability /{s/^$capability //;q};d")
        capability_values=$(echo "$capability_values" | sed "/^$capability /{s/.*//; :p ;n; bp }")

        if [ -z "$input" ]; then
            input="0"
        else
            validate_value "$capability" "$conf_values" "$input" ||
                exit 1
        fi
    fi

    char_input="$(input_to_char "$capability" "$conf_values" "$input")"

    case "$value_types" in
        *"several"*)
            if [ "$char_input" != "0" ]; then
                dependent_capability="$(json_get "$values_file" "$capabilities_path.several.capability")"

                for module_num in $(seq "$input"); do
                    get_capability_value \
                        "$is_interactive" "${dependent_capability/"%i"/"$module_num"}" "$capabilities_path.several"
                    char_input="${char_input}${result_part}"
                done
            fi
            ;;

        *"repeat"*)
            dependent_capability="$(json_get "$values_file" "$capabilities_path.repeat.capability")"
            if [ "$char_input" = "0" ]; then
                char_input="${char_input}0"
            else
                get_capability_value \
                    "$is_interactive" "${dependent_capability/"%i"/""}" "$capabilities_path.repeat"
                char_input="${char_input}${result_part}"
            fi
            ;;
    esac

    result_part="$char_input"
}

get_pn_part() {
    local capabilities="$1"
    local result=""

    for capability in $capabilities; do
        get_capability_value "$interactive_mode" "$capability" "" || exit $?
        result="${result}${result_part}"
    done

    pn_part="$result"
}

generate_partnumber () {
    local device_number="$1"
    local base_capabilities="$2"
    local device_capabilities="$3"

    get_pn_part "$base_capabilities" || exit $?
    local base_input="$pn_part"

    get_pn_part "$device_capabilities" || exit $?
    local device_input="$pn_part"

    partnumber="$(put_together "$device_number" "$base_input" "$device_input")"

    [ -n "$capability_values" ] && {
        capability_values=$(echo "$capability_values" | sed "/^$/d")
        print_dash
        echo "WARNING: Some capabilities were not used${NL}Unused capabilities:${NL}" >&2
        echo "$capability_values" >&2
        echo "${NL}ADVICE: $0 -h $device_number" >&2
        print_dash
    }

    echo "$partnumber"
}

# Build partnumber
pn_compress() {
    local string="$1"

    local zero_count=0
    local result=""
    local char=""

    for char_num in $(seq "${#string}"); do
        char=$(echo "$string" | cut -c "$char_num")

        if [ "$char" = "0" ]; then
            zero_count=$((zero_count + 1))
        else
            [ $zero_count -eq 1 ] && result="${result}0"
            [ $zero_count -gt 1 ] && result="${result}${zero_count}"
            result="${result}${char}"
            zero_count=0
        fi
    done

    result=$(echo "$result" | sed 's/[0-9]*$//')

    echo "$result"
}

put_together() {
    # FAMILY_SERIES-BASE.OPT
    local device_number="$1"
    local base="$2"
    local specs="$3"
    local partnumber=""

    base=$(pn_compress "$base")
    specs=$(pn_compress "$specs")

    if [ -n "$specs" ] && [ -n "$base" ]; then
        partnumber="${device_number}-${base}.${specs}" 
    elif [ -n "$base" ]; then
        partnumber="${device_number}-${base}"
    else 
        partnumber=""
    fi

    echo "$partnumber"
}

# Main
partnumber () {
    local conf_file="$(safe_cat "$conf_dir/devices.json")"
    local devices_list="$(json_get_keys "$conf_file" "@.devices.*.opt")"

    process_options "$devices_list" "$@"
    shift $shifts
    [ "$interactive_mode" = true ] && input_device "$devices_list"

    local base_file
    local dev_file
    base_file="$(get_device_config "$conf_file" "base")"  || exit $?
    dev_file="$(get_device_config "$conf_file" "device")" || exit $?
    values_file="$(get_device_config "$conf_file" "values")" || exit $?

    get_arg_capabilities "$@"
    local base_capabilities="$(json_get "$base_file" '@.capabilities[*]')"
    local device_capabilities="$(json_get "$dev_file" '@.capabilities[*]')"

    if [ "$description_mode" = true ]; then
        print_capabilities_list "$base_capabilities" "$device_capabilities"
        exit 0
    fi

    local partnumber
    partnumber="$(generate_partnumber \
        "$device_number" "$base_capabilities" "$device_capabilities")" || exit $?

    [ -z "$partnumber" ] && {
        echo "Error: Missing capabilities or Interactive mode" >&2
        exit 1
    }

    echo -en "\nGenerated Partnumber: " >&2
    echo "$partnumber"
}

partnumber "$@"