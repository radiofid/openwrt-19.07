#!/bin/sh

tmpfile="ftpcommands.tmp"
file=$1
dir=$2
[ -z "$FTP_HOST" -o -z "$FTP_USER" -o -z "$FTP_PASS" ] && exit 0

doom() {
    echo "open $FTP_HOST"        > $tmpfile
    echo "user $FTP_USER $FTP_PASS" >> $tmpfile
    [ "$1" = "check" ] && echo "ls" >> $tmpfile || echo "put $dir/$file $file" >> $tmpfile
    echo "bye" >> $tmpfile
    [ "$1" = "check" ] && {
        echo "$(ftp -n < $tmpfile | awk '{print $9}')"
    } || ftp -n < $tmpfile
}

[ "$(doom check | grep $file)" = "$file" ] || doom put
rm $tmpfile
